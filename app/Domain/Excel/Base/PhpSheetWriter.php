<?php

namespace App\Domain\Excel\Base;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Writer\Exception;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class PhpSheetWriter
{
    private Xlsx $writer;
    private Worksheet $sheet;
    private array $headers;

    public function __construct(
        private readonly Spreadsheet $spreadsheet
    ) {
        $this->writer = new Xlsx($this->spreadsheet);
        $this->sheet = $this->spreadsheet->getActiveSheet();
    }

    /**
     * @param array $headers
     * @return $this
     */
    public function setHeader(array $headers): static
    {
        $this->headers = $headers;

        $this->setCells($headers);

        return $this;
    }

    /**
     * @param array $item
     * @return string[]
     */
    public function getHeadersFromKeys(array $item): array
    {
        if (!empty($item)) {
            return [];
        }

        $keys = array_keys($item);

        return collect($keys)->map(function ($item){
            return Str::of($item)->explode('_')->map(function ($item){
               return Str::of($item)->ucfirst();
            })->join(' ');
        })->toArray();
    }

    /**
     * @param array $items
     * @return $this
     */
    public function setItems(array $items): static
    {
        foreach ($items as $key => $item) {
            $row = ($this->headers) ? $key+2 : $key+1;

            $this->setCells($item, $row);
        }

        return $this;
    }

    /**
     * @param string $fileName
     * @param string $disk
     * @return string
     * @throws Exception
     */
    public function export(string $fileName, string $disk = 'system'): string
    {
        $path = Storage::disk($disk)->path($fileName);

        $this->writer->save($path);

        return $path;
    }

    private function setCells(array $data, int $row = 1): void
    {
        $alphabet = range('A', 'Z');
        $data = isset($data[0]) ? $data : array_values($data);

        foreach ($data as $key => $value) {
            $letter = $alphabet[$key];

            $this->sheet->setCellValueExplicit($letter . $row, $value, DataType::TYPE_STRING2);
        }
    }

}
