<?php /** @noinspection PhpUnused */

namespace App\Domain\Excel\Base;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\Reader\Exception;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class PhpSheetReader
{

    private Xlsx $reader;
    private Spreadsheet $spreadsheet;
    private Worksheet $worksheet;
    public int $highestRow;
    public string $highestColumn;
    public int $highestColumnIndex;

    private array $keys;

    /**
     * @param string $file
     * @param string $disk
     * @throws Exception|\PhpOffice\PhpSpreadsheet\Exception
     */
    public function __construct(string $file, string $disk = 'system')
    {
        $path = Storage::disk($disk)->path($file);

        $this->reader = new Xlsx();
        $this->reader->setReadDataOnly(true);
        $this->spreadsheet = $this->reader->load($path);
        $this->worksheet = $this->spreadsheet->getActiveSheet();

        $this->highestRow = $this->worksheet->getHighestRow(); // e.g. 10
        $this->highestColumn = $this->worksheet->getHighestColumn(); // e.g 'F'
        $this->highestColumnIndex = Coordinate::columnIndexFromString($this->highestColumn);

        $this->autoSetKeys();
    }

    protected function autoSetKeys()
    {
        $keys = [];
        for ($col = 1; $col <= $this->highestColumnIndex; ++$col) {
            $header = $this->worksheet->getCell([$col, 1])->getValue();

            $keys[] = Str::snake($header);
        }
        $this->keys = $keys;
    }

    /**
     * @return array
     */
    public function getKeys(): array
    {
        return $this->keys;
    }

    /**
     * @param array $keys
     */
    public function setKeys(array $keys): void
    {
        $this->keys = $keys;
    }

    public function getEntities(int $offset = 0, int $count = 0)
    {
        $start = $offset + 2;
        $last = $count ? $start + ($count - 1) : $this->highestRow;
        if ($last > $this->highestRow) {
            $last = $this->highestRow;
        }

        $entries = [];
        for ($row = $start; $row <= $last; ++$row) {
            $entry = [];
            for ($col = 1; $col <= $this->highestColumnIndex; ++$col) {
                $key = $this->keys[$col - 1];
                $entry[$key] = $this->worksheet->getCell([$col, $row])->getValue();
            }
            $entries[] = $entry;
        }

        return $entries;
    }

    /**
     * @return int
     */
    public function getHighestRow(): int
    {
        return $this->highestRow;
    }

    /**
     * @param int $highestRow
     */
    public function setHighestRow(int $highestRow): void
    {
        $this->highestRow = $highestRow;
    }

}
