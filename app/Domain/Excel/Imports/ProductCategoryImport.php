<?php

namespace App\Domain\Excel\Imports;

use App\Domain\Excel\Imports\Base\CustomImport;
use App\Models\ProductCategory;

class ProductCategoryImport extends CustomImport
{

    public static function model(array $row) {
        return new ProductCategory([
            'name' => $row['chat_bot_product_category'],
            'trading_agent_code' => $row['trading_agent_code'],
            'trade_point_code' => $row['trade_point_code'],
            'full_name' => $row['product_category'],
        ]);
    }

    public static function saveMany(array $data) {
        ProductCategory::insert($data);
    }

    public function chunkSize(): int
    {
        return 300;
    }

    public function truncate() {
        ProductCategory::truncate();
    }

}
