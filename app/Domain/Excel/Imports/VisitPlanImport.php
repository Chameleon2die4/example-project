<?php

namespace App\Domain\Excel\Imports;

use App\Domain\Excel\Imports\Base\CustomImport;
use App\Models\VisitPlan;

class VisitPlanImport extends CustomImport
{

    public static function model(array $row)
    {
        $row['fio'] = $row['trading_agent'];
        $row['phone'] = intval($row['trading_agent_phone']);

        return new VisitPlan($row);
    }

    public static function saveMany(array $data) {
        VisitPlan::insert($data);
    }

    public function chunkSize(): int
    {
        return 300;
    }

    public function truncate() {
        VisitPlan::truncate();
    }

}
