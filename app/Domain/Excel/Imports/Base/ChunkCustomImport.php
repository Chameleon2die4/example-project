<?php

namespace App\Domain\Excel\Imports\Base;

use App\Domain\Excel\Base\PhpSheetReader;
use Illuminate\Bus\Batchable;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use PhpOffice\PhpSpreadsheet\Exception;

class ChunkCustomImport implements ShouldQueue
{
    use Batchable, Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private int $offset;
    private int $count;
    private string $import;
    private string $file;
    private string $disk;

    /**
     * Create a new job instance.
     *
     * @param string $import
     * @param string $file
     * @param string $disk
     * @param int $offset
     * @param int $count
     */
    public function __construct(string $import, string $file, string $disk, int $offset, int $count)
    {
        $this->import = $import;
        $this->offset = $offset;
        $this->count = $count;
        $this->file = $file;
        $this->disk = $disk;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     */
    public function handle()
    {
        $import = $this->import;
        $reader = new PhpSheetReader($this->file, $this->disk);
        $entities = $reader->getEntities($this->offset, $this->count);

        $models = collect();
        foreach ($entities as $entity) {
            /** @noinspection PhpUndefinedMethodInspection */
            $models->push($import::model($entity));
        }

        if (!empty($models)) {
            $data = $models->toArray();

            /** @noinspection PhpUndefinedMethodInspection */
            $import::saveMany($data);
//            DB::table($model->getTable())->insert($data);
        }
    }
}
