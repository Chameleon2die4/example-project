<?php

namespace App\Domain\Excel\Imports\Base;

use App\Domain\Excel\Base\PhpSheetReader;
use App\Jobs\AddImportLog;
use Illuminate\Bus\Batchable;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Bus;
use PhpOffice\PhpSpreadsheet\Exception;
use Throwable;

abstract class CustomImport implements ShouldQueue
{
    use Batchable, Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public string $file;
    public string $disk;

    /**
     * Create a new job instance.
     *
     * @param string $file
     * @param string $disk
     */
    public function __construct(string $file, string $disk = 'system')
    {
        $this->file = $file;
        $this->disk = $disk;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws Throwable
     * @throws Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     */
    public function handle()
    {
        $reader = new PhpSheetReader($this->file, $this->disk);

        $size = $this->chunkSize();
        $last = $reader->getHighestRow();
        $total = $last - 1;

        $chunks = ceil($total / $size);

        $this->truncate();

        $events = [];
        for ($i = 0; $i < $chunks; $i++) {
            $offset = $i * $size;
            $events[] = new ChunkCustomImport(get_class($this), $this->file, $this->disk, $offset, $size);
        }

        $file = $this->file;
        Bus::batch($events)->name('ChunkCustomImport')
            ->then(function () use ($file) {
                AddImportLog::dispatch($file);
            })
//            ->onQueue('background')
            ->dispatch();
    }

    public function chunkSize(): int
    {
        return 500;
    }

    public function truncate()
    {

    }

    abstract static public function model(array $row);

    abstract static public function saveMany(array $data);

}
