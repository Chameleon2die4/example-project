<?php

namespace App\Domain\Excel\Console\Commands;

use App\Jobs\AddImportLog;
use App\Jobs\SetRelations;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\Console\Command\Command as BaseCommand;

/**
 * @deprecated 25.03.2024
 */
class ImportVisitsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:visits';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import product categories and visits agents';

    /**
     * Execute the console command.
     *
     * @return int
     * @noinspection DuplicatedCode
     */
//    public function handle()
//    {
//        $this->info('Import files start...');
//        $files = self::getFiles();
//        $disk = config('filesystems.file_disk');
//
//        foreach ($files as $filename => $modelName) {
//            if (Storage::disk($disk)->exists($filename)) {
//                $content = Storage::disk($disk)->get($filename);
//                Storage::disk('system')->put('temp/'.$filename, $content);
//
//                $model = "App\\Models\\$modelName";
//                if (class_exists($model)) {
//                    $model::truncate();
//                    $import = "App\\Imports\\{$modelName}Import";
//
//                    if (class_exists($import)) {
//                        (new $import)->queue('temp/'.$filename, 'system')->chain([
//                            new AddImportLog($filename),
//                            new SetRelations($model),
//                        ]);
//                    }
//                }
//            }
//        }
//
//        $this->info('Import files processing...');
//
//        return BaseCommand::SUCCESS;
//    }
    public function handle()
    {
        $this->info('Import files start...');
        $files = self::getFiles();
        $disk = config('filesystems.file_disk');

        $events = [];
        foreach ($files as $filename => $modelName) {
            if (Storage::disk($disk)->exists($filename)) {
                $content = Storage::disk($disk)->get($filename);
                Storage::disk('system')->put('temp/' . $filename, $content);

                $model = "App\\Models\\$modelName";
                if (class_exists($model)) {
                    $import = "App\\Domain\\Excel\\Imports\\{$modelName}Import";

                    if (class_exists($import)) {
                        $events[] = new $import('temp/' . $filename);
                    }
                }
            }
        }

        Bus::batch($events)->name('ImportVisitsPCat')
            ->then(function () use ($files, $disk) {
                $events = [];
                foreach ($files as $filename => $modelName) {
                    if (Storage::disk($disk)->exists($filename)) {
                        $model = "App\\Models\\$modelName";

                        $events[] = new AddImportLog($filename);
                        $events[] = new SetRelations($model);
                    }
                }

                Bus::batch($events)->name('AddImportLogs')->dispatch();
            })
            ->dispatch();


        $this->info('Import files processing...');

        return BaseCommand::SUCCESS;
    }

    /**
     * @return string[]
     */
    public static function getFiles(): array
    {
        return [
//            'product_categories.xlsx'        => 'ProductCategory',
//            'trading_agents_visit_plan.xlsx' => 'VisitPlan',
        ];
    }
}
