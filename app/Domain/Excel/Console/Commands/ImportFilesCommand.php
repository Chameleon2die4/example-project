<?php

namespace App\Domain\Excel\Console\Commands;

//use App\Domain\User\Notifications\AdminNotification;
use App\Jobs\AddImportLog;
//use App\Jobs\SetRelations;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\Console\Command\Command as BaseCommand;

class ImportFilesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:files';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import files command';

    private array $ext = ['csv', 'xlsx'];

//    private AdminNotification $adminNotification;
//
//    /**
//     * @param AdminNotification $adminNotification
//     */
//    public function __construct(AdminNotification $adminNotification)
//    {
//        parent::__construct();
//
//        $this->adminNotification = $adminNotification;
//    }

    /**
     * Execute the console command.
     *
     * @return int
     * @noinspection DuplicatedCode
     */
    public function handle(): int
    {
        $this->info('Import files start...');
        $files = self::getFiles();
        $disk = config('filesystems.file_disk');

        foreach ($files as $filename => $modelName) {

            foreach ($this->ext as $ext) {
                // Todo Temp fix
                if ($filename === 'order_details' && $ext === 'xlsx') {
                    continue;
                }

                $file = $filename . '.' . $ext;
                if (Storage::disk($disk)->exists($file)) {
//                $content = Storage::disk($disk)->get($filename);
//                $filename = 'temp/' . $filename;
//                Storage::disk('system')->put($filename, $content);

                    $model = "App\\Models\\$modelName";
                    if (class_exists($model)) {
                        $model::truncate();
                        $import = "App\\Imports\\{$modelName}Import";

                        if (class_exists($import)) {
                            (new $import)->queue($file, $disk)->chain([
                                new AddImportLog($file),
//                                new SetRelations($model),
                            ]);
                        }
                    }
                }
            }
        }

        $this->info('Import files processing...');

        return BaseCommand::SUCCESS;
    }

    /**
     * @return string[]
     */
    public static function getFiles(): array
    {
        return [
            'vat_rates'              => 'VatRate',
            'warehouses'             => 'Warehouse',
            'nomenclature_types'     => 'NomenclatureType',
            'measurement_units'      => 'MeasurementUnit',
            'trade_agents'           => 'TradeAgent',
//            'trading_points'         => 'TradingPoint',
            'contact_persons'        => 'ContactPerson',
            'nomenclature'           => 'Nomenclature',
            'stock_balances'         => 'StockBalance',
//            'client_orders'          => 'ClientOrder',
            'order_details'          => 'OrderDetail',
            'remaining_bonus_points' => 'BonusPoint',
            'promo'                  => 'SpecialProposition',
            'trading_point_promo'    => 'PropositionTradingPoint',
            'promo_brand'            => 'PromoBrand',
            'promo_category'         => 'PromoCategory',
            'clients_debt'           => 'Balance',
        ];
    }
}
