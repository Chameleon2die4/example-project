<?php

namespace App\Domain\Excel\Console\Commands;

use App\Jobs\AddImportLog;
use App\Models\ImportLog;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\Console\Command\Command as BaseCommand;

class ImportNewCommand extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:new';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info('Import files start...');
        $map = self::getFileMap();
        $disk = config('filesystems.file_disk');
        $directory = 'NewTP';
        $files = Storage::disk($disk)->files($directory);

        foreach ($files as $file) {
            preg_match('/^([a-z_]*)([0-9_-]*)?.(xlsx|csv)/', basename($file), $matches);
            $log = ImportLog::whereFile($file)->whereType('quick')->first();

            if (count($matches) > 1 && isset($map[$matches[1]]) && !$log) {
                $modelName = $map[$matches[1]];

                $model = "App\\Models\\$modelName";
                if (class_exists($model)) {
                    $import = "App\\Imports\\{$modelName}Import";

                    if (class_exists($import)) {
                        (new $import)->queue($file, $disk)->allOnQueue('new')->chain([
                            new AddImportLog($file, 'quick'),
                        ]);
                    }
                }
            }
        }

        $this->info('Import files processing...');

        return BaseCommand::SUCCESS;
    }

    /**
     * @return string[]
     */
    public static function getFileMap(): array
    {
        return [
            'trading_points_new' => 'TradingPoint',
            'contact_persons_new' => 'ContactPerson',
            'messages' => 'Notice',
            'trading_point_messages' => 'NoticeTradingPoint',
        ];
    }
}
