<?php

namespace App\Domain\Excel\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\Console\Command\Command as BaseCommand;

class GenerateNewFilesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:new';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate new files for import';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $disk = config('filesystems.file_disk');
        $sources = 'new';
        $dest = 'NewTP';
        $files = Storage::disk($disk)->files($sources);
        $timestamp = Carbon::now()->format('Ymd_his');

        foreach ($files as $file) {
            $fileName = basename($file);
            $exp = explode('.', $fileName);
            $new = $exp[0] . $timestamp . '.' . $exp[1];

            Storage::disk('system')->copy($file, $dest . '/' . $new);
        }

        $this->info('New file generated!');

        return BaseCommand::SUCCESS;
    }
}
