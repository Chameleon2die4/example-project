<?php

declare(strict_types=1);

namespace App\Domain\ProductCategory\Actions;

use App\Domain\Base\Actions\Contracts\AsControllerActionableInterface;
use App\Models\ProductCategory;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use function response;

class ProductCategoryImportAction implements AsControllerActionableInterface
{
    /**
     * @param Request $request
     * @return Application|\Illuminate\Foundation\Application|\Illuminate\Http\Response|ResponseFactory
     */
    public function asController(Request $request)
    {
        $rows = $request->all();
        $insertRows = [];

        foreach ($rows as $row) {
            if (empty($row['chat_bot_product_category'])) {
                continue;
            }

            $insertRows[] = [
                'name' => $row['chat_bot_product_category'],
                'trading_agent_code' => $row['trading_agent_code'],
                'trade_point_code' => $row['trade_point_code'],
                'full_name' => $row['product_category'],
            ];
        }

        if (empty($insertRows)) {
            return response([
                'message' => 'No product categories inserted.',
                'request' => $rows
            ], Response::HTTP_NO_CONTENT);
        }

        ProductCategory::insert($insertRows);
        $count = count($insertRows);

        return response([
            'message' => "Inserted {$count} product categories."
        ], Response::HTTP_CREATED);
    }
}
