<?php
declare(strict_types=1);

namespace App\Domain\TelegramBot\Commands;

use App\Domain\TelegramBot\Actions\TelegramSendMessageAction;
use App\Models\User;

abstract class AbstractSendMessage
{
    /**
     * @var int
     */
    public int $chatId;

    public ?User $user = null;

    /**
     * @param TelegramSendMessageAction $telegramSendMessageAction
     */
    public function __construct(private readonly TelegramSendMessageAction $telegramSendMessageAction)
    {
    }

    /**
     * @param string $message
     *
     * @return void
     */
    public function sendMessage(string $message): void
    {
        $this->telegramSendMessageAction
            ->setChatId($this->chatId)
            ->setUser($this->user)
            ->setText($message)
            ->execute();
    }

    /**
     * @param string $message
     * @param array $options
     *
     * @return void
     */
    public function sendMessageWithOptions(string $message, array $options): void
    {
        $this->telegramSendMessageAction
            ->setOptions($options)
            ->setChatId($this->chatId)
            ->setUser($this->user)
            ->setText($message)
            ->execute();
    }

    /**
     * @param int $chatId
     */
    public function setChatId(int $chatId): void
    {
        $this->chatId = $chatId;
    }

    /**
     * @param User|null $user
     */
    public function setUser(?User $user): static
    {
        $this->user = $user;

        return $this;
    }
}
