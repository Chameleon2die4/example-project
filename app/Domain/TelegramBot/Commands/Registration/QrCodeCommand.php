<?php
declare(strict_types=1);

namespace App\Domain\TelegramBot\Commands\Registration;

use App\Domain\TelegramBot\Commands\AbstractSendMessage;
use App\Domain\TelegramBot\Contracts\TelegramRegistrationCommandInterface;
use Illuminate\Database\Eloquent\Collection;

class QrCodeCommand extends AbstractSendMessage implements TelegramRegistrationCommandInterface
{
    /**
     * @var Collection|null
     */
    protected ?Collection $tradingPoints;

    public function execute(int $chatId): void
    {
        $this->chatId = $chatId;

        $params = [];

        if (!$this->tradingPoints) {
            return;
        }

        foreach ($this->tradingPoints as $tradingPoint) {
            $params[] = [ ['text' => $tradingPoint->name, 'callback_data' => $tradingPoint->code] ];
        }

        $this->sendMessageWithOptions(trans('telegram.select_trading_point'), [
            'params' => $params,
            'is_keyboard' => true,
            'is_inline' => true,
            'resize_keyboard' => true,
        ]);
    }

    /**
     * @param Collection |null $tradingPoints
     *
     * @return $this
     */
    public function setEntity(?Collection $tradingPoints): static
    {
        $this->tradingPoints = $tradingPoints;

        return $this;
    }
}
