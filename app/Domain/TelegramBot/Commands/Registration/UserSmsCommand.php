<?php
declare(strict_types=1);

namespace App\Domain\TelegramBot\Commands\Registration;

use App\Domain\SmsClient\SmsClient;
use App\Domain\TelegramBot\Actions\TelegramSendMessageAction;
use App\Domain\TelegramBot\Enums\ResponseStatusEnum;
use App\Domain\TelegramBot\Enums\RegistrationStepsEnum;
use App\Domain\TelegramBot\Traits\SendMessageTrait;
use App\Domain\TelegramBot\Traits\TelegramUserSessionTrait;
use App\Domain\User\Actions\UserUpdateAction;
use App\Models\ContactPerson;
use App\Models\User;
use Throwable;

class UserSmsCommand
{
    use SendMessageTrait;
    use TelegramUserSessionTrait;

    /**
     * @var ContactPerson|null
     */
    private ?ContactPerson $contactPerson;

    private array $message;

    /**
     * @param UserUpdateAction $userUpdateAction
     * @param TelegramSendMessageAction $telegramSendMessageAction
     * @param SmsClient $smsClient
     */
    public function __construct(
        private readonly UserUpdateAction          $userUpdateAction,
        private readonly TelegramSendMessageAction $telegramSendMessageAction,
        private readonly SmsClient                 $smsClient
    ) { }

    /**
     * @param User|null $user
     *
     * @return void
     * @throws Throwable
     */
    public function execute(?User $user): void
    {
        if (!$user || !$this->contactPerson?->phone) {
            return;
        }

        $this->chatId = $user->chat_id;
        $code = $this->smsClient->generateSmsCode();
        $this->userUpdateAction
            ->setContactPerson($this->contactPerson)
            ->setUser($user)
            ->setToken((string)$code)
            ->setVerify()
            ->execute();

        $this->smsClient->execute("BaltonBot Code: $code", (string)$this->contactPerson->phone);

        $this->setSession([
            'message' => $this->message['text'] ?? null,
            'chatId' => $this->chatId,
            'botType' => $this->message['botType'],
            'response' => $this->message['response'],
            'status' => ResponseStatusEnum::SUCCESS,
            'type' => RegistrationStepsEnum::SMS_VERIFY,
        ]);

        $this->sendMessage(trans('telegram.send_sms_verify_code'));
    }

    /**
     * @param ContactPerson|null $contactPerson
     *
     * @return $this
     */
    public function setContactPerson(?ContactPerson $contactPerson): static
    {
        $this->contactPerson = $contactPerson;

        return $this;
    }

    /**
     * @param array $message
     *
     * @return $this
     */
    public function setMessage(array $message): static
    {
        $this->message = $message;

        return $this;
    }
}
