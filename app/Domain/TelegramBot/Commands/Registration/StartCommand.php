<?php
declare(strict_types=1);

namespace App\Domain\TelegramBot\Commands\Registration;

use App\Domain\TelegramBot\Actions\TelegramSendMessageAction;
use App\Domain\TelegramBot\Enums\BotMenuCallbackTypeEnum;
use App\Domain\TelegramBot\Traits\SendMessageTrait;

class StartCommand
{
    use SendMessageTrait;

    /**
     * @param TelegramSendMessageAction $telegramSendMessageAction
     */
    public function __construct(private readonly TelegramSendMessageAction $telegramSendMessageAction)
    {
    }

    /**
     * @param int $chatId
     *
     * @return void
     */
    public function execute(int $chatId): void
    {
        $this->chatId = $chatId;

        $this->sendMessage(trans('telegram.welcome'));

        $this->sendMessageWithOptions(trans('telegram.start_message'), [
            'params' => [
                [
                    ['text' => trans('menu.lang_uz'), 'callback_data' => BotMenuCallbackTypeEnum::LANG_UZ->value],
                    ['text' => trans('menu.lang_ru'), 'callback_data' => BotMenuCallbackTypeEnum::LANG_RU->value]
                ],
            ],
            'is_keyboard' => true,
            'is_inline' => false,
            'resize_keyboard' => true,
        ]);
    }
}
