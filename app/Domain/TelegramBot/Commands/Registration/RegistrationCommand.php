<?php
declare(strict_types=1);

namespace App\Domain\TelegramBot\Commands\Registration;

use App\Domain\TelegramBot\Actions\TelegramSendMessageAction;
use App\Domain\TelegramBot\Traits\SendMessageTrait;
use App\Domain\TradingPoints\Actions\TradingPointsAction;
use App\Domain\User\Actions\UserRegistrationAction;
use App\Models\TradingPoint;
use Throwable;

class RegistrationCommand
{
    use SendMessageTrait;

    /**
     * @param UserRegistrationAction $userRegistrationAction
     * @param TelegramSendMessageAction $telegramSendMessageAction
     * @param TradingPointsAction $tradingPointsAction
     */
    public function __construct(
        private readonly UserRegistrationAction $userRegistrationAction,
        private readonly TelegramSendMessageAction $telegramSendMessageAction,
        private readonly TradingPointsAction $tradingPointsAction,
    ) {
    }

    /**
     * @param int $chatId
     * @param string|null $code
     *
     * @return void
     * @throws Throwable
     */
    public function execute(int $chatId, ?string $code): void
    {
        $this->chatId = $chatId;

        if (!$code) {
            $this->sendMessage(trans('telegram.phone_wrong'));

            $this->sendMessage(trans('telegram.send_your_phone_number'));

            return;
        }

        $tradingPoint = TradingPoint::whereCode($code)->first();

        if (!$tradingPoint) {
            $this->sendMessage(trans('telegram.trade_mark_info_error') . $code);

            return;
        }

        $text = trans('telegram.trade_mark_info', [
            'trade_mark' => $tradingPoint->name,
            'address'    => $tradingPoint->address
        ]);
        $this->sendMessage($text);

        $this->sendMessage(trans('telegram.send_your_phone_number'));

        $this->userRegistrationAction
            ->setChatId($chatId)
            ->setCode($code)
            ->execute();
    }
}
