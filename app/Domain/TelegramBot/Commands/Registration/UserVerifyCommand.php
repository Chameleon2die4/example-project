<?php
declare(strict_types=1);

namespace App\Domain\TelegramBot\Commands\Registration;

use App\Domain\SmsClient\SmsClient;
use App\Domain\TelegramBot\Actions\TelegramSendMessageAction;
use App\Domain\TelegramBot\Enums\RegistrationStepsEnum;
use App\Domain\TelegramBot\Events\TelegramRegistrationLogEvent;
use App\Domain\TelegramBot\Traits\SendMessageTrait;
use App\Domain\User\Actions\UserUpdateAction;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Telegram\Bot\Objects\Update as UpdateObject;
use Throwable;

class UserVerifyCommand
{
    use SendMessageTrait;

    private const SUCCESS_CODE = 1;

    /**
     * @param UserUpdateAction $userUpdateAction
     * @param TelegramSendMessageAction $telegramSendMessageAction
     * @param SmsClient $smsClient
     */
    public function __construct(
        private readonly UserUpdateAction          $userUpdateAction,
        private readonly TelegramSendMessageAction $telegramSendMessageAction,
        private readonly SmsClient                 $smsClient

    ) { }

    /**
     * @param ?User $user
     * @param UpdateObject $updates
     *
     * @return void
     * @throws Throwable
     */
    public function execute(?User $user, UpdateObject $updates): void
    {
        if (!$user) {
            return;
        }

        $message = $updates->getMessage();
        $this->chatId = $user->chat_id;

//        event(new TelegramRegistrationLogEvent([
//            'user_id' => $user->id,
//            'chat_id' => $this->chatId,
//            'response' => json_encode([
//                'sms_verify_success' => trans('telegram.registration_success'),
//                'menu_start' => trans('telegram.main_menu_text'),
//                'remember_token' => $user->remember_token,
//            ]),
//            'step' => RegistrationStepsEnum::SMS_VERIFY->value,
//            'message' => $message->text,
//        ]));

        if (Hash::check($message->text, $user->remember_token) != self::SUCCESS_CODE) {
            if ($message->text !== trans('telegram.send_sms_replay')) {
                $text = trans('telegram.send_sms_verify_code_incorrect');
                $this->sendMessageWithOptions($text, [
                    'params' => [ [['text' => trans('telegram.send_sms_replay')]] ],
                    'is_keyboard' => true,
                    'is_inline' => false,
                    'resize_keyboard' => true,
                ]);

                $this->sendMessage(trans('telegram.send_sms_verify_code'));
            }

            if ($message->text === trans('telegram.send_sms_replay')) {
                $code = $this->smsClient->generateSmsCode();

                $this->userUpdateAction
                    ->setUser($user)
                    ->setContactPerson()
                    ->setVerify()
                    ->setToken((string)$code)
                    ->execute();

                $status = $this->smsClient->execute("Code: $code", (string)$user->tel);

                $key = $status ? 'telegram.send_sms_verify_code' : 'telegram.send_sms_verify_code_error';
                $this->sendMessage(trans($key));
            }

            return;
        }

        $this->userUpdateAction
            ->setUser($user)
            ->setContactPerson(null)
            ->setVerify(self::SUCCESS_CODE)
            ->setToken($message->text)
            ->execute();

        $user->refresh();
        $text = trans('telegram.registration_success', ['name' => $user->name]);

        $this->sendMessageWithOptions($text, [
            'params' => [
                [ ['text' => trans('menu.visits_agent')], ['text' => trans('menu.bonus')] ],
                [ ['text' => trans('menu.orders')], ['text' => trans('menu.special_proposition')] ],
                [ ['text' => trans('menu.support')] ],
            ],
            'is_keyboard' => true,
            'is_inline' => false,
            'resize_keyboard' => true,
        ]);

        $user->clearTelegramSession();
    }
}
