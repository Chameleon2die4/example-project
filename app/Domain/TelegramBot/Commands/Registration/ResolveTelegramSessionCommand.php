<?php
declare(strict_types=1);

namespace App\Domain\TelegramBot\Commands\Registration;

use App\Domain\ContactPerson\Action\ContactPersonsAction;
use App\Domain\TelegramBot\Enums\ResponseStatusEnum;
use App\Domain\TelegramBot\Enums\RegistrationStepsEnum;
use App\Domain\TelegramBot\Traits\TelegramUserSessionTrait;
use App\Domain\TradingPoints\Actions\TradingPointsAction;
use App\Models\ContactPerson;
use App\Models\TradingPoint;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Telegram\Bot\Objects\Update as UpdateObject;

class ResolveTelegramSessionCommand
{
    use TelegramUserSessionTrait;

    private UpdateObject $update;

    public ?User $user;

    private const MAIN_COUNTRY_MAX_NUMBER = 9;

    /**
     * @param ContactPersonsAction $contactPersonsAction
     * @param TradingPointsAction $tradingPointsAction
     */
    public function __construct(
        private readonly ContactPersonsAction $contactPersonsAction,
        private readonly TradingPointsAction  $tradingPointsAction,
    ) {
    }

    /**
     * @return ContactPerson|TradingPoint[]|array|Builder|Builder[]|Collection|Model|null
     */
    public function execute(): Model|Collection|ContactPerson|array|Builder|null
    {
        $chat = $this->update->getChat();
        $message = $this->update->getMessage();
        $messageText = $message->isEmpty() ? '' : $message->text;
        $messagePhone = '';

        if ($this->update->callbackQuery?->data) {
            $this->setSession([
                'message' => $messageText,
                'chatId' => $chat->id,
                'botType' => $message->objectType(),
                'response' => json_encode($message->getRawResponse()),
                'status' => ResponseStatusEnum::SUCCESS,
                'type' => RegistrationStepsEnum::PROVIDER,
            ]);
        }

        $tradingPoint = $this->tradingPointsAction->getByQrCode($messageText);
        $isCollectionTradingPoint = $tradingPoint && count($tradingPoint) > 1;

        if ($isCollectionTradingPoint) {
                $this->setSession([
                    'message' => $messageText,
                    'chatId' => $chat->id,
                    'botType' => $message->objectType(),
                    'response' => json_encode($message->getRawResponse()),
                    'status' => ResponseStatusEnum::SUCCESS,
                    'type' => RegistrationStepsEnum::QR_CODES,
                ]);

                return $tradingPoint;
        } else {
            $tradingPoint = $this->tradingPointsAction->getByCode($this->update->callbackQuery?->data);

            if (!$tradingPoint && is_numeric($messageText)) {
                $tradingPoint = $this->tradingPointsAction->getByQrCode($messageText)?->first();
            }
        }

        if ($tradingPoint) {
            $contactPerson = $this->contactPersonsAction->getContactPersonByCode($tradingPoint->code);
            $userType = $this->user && $this->user?->code === $tradingPoint->code ? 'phone' : 'provider';

            $this->setSession([
                'message' => $messageText,
                'chatId' => $chat->id,
                'botType' => $message->objectType(),
                'response' => json_encode($message->getRawResponse()),
                'status' => ResponseStatusEnum::SUCCESS,
                'type' => RegistrationStepsEnum::from($userType),
            ]);

            return $contactPerson;
        }

        if ($messageText !== null && strlen($messageText) === self::MAIN_COUNTRY_MAX_NUMBER) {
            $messagePhone = 998 . $message->text;
        }

        $contactPersonPhone = $this->contactPersonsAction->getContactPersonByPhone($messagePhone ?: $messageText, $this->user);

        if ($contactPersonPhone) {
            $this->setSession([
                'message' => $messageText,
                'chatId' => $chat->id,
                'botType' => $message->objectType(),
                'response' => json_encode($message->getRawResponse()),
                'status' => ResponseStatusEnum::SUCCESS,
                'type' => RegistrationStepsEnum::PHONE,
            ]);

            return $contactPersonPhone;
        }

        return null;
    }

    public function setUpdate(UpdateObject $update): static
    {
        $this->update = $update;

        return $this;
    }

    public function setUser(?User $user): static
    {
        $this->user = $user;

        return $this;
    }
}
