<?php
declare(strict_types=1);

namespace App\Domain\TelegramBot\Commands\Special;

use App\Domain\TelegramBot\Commands\AbstractSendMessage;
use App\Models\User;

class NotificationCommand extends AbstractSendMessage
{
    public function execute(string $message, int $chatId, ?User $user = null) {
        $this->chatId = $chatId;
        $this->user = $user;

        $this->sendMessage($message);
    }
}
