<?php
declare(strict_types=1);

namespace App\Domain\TelegramBot\Commands\Menu;

use App\Domain\TelegramBot\Base\BotMenuMessage;
use App\Domain\TelegramBot\Commands\AbstractSendMessage;
use App\Domain\TelegramBot\Contracts\TelegramMenuCommandInterface;
use App\Domain\TelegramBot\Enums\BotMenuCallbackTypeEnum;
use App\Domain\TelegramBot\Traits\TelegramUserSessionTrait;
use App\Models\User;
use App\Models\VisitPlan;

class VisitPlanCommand extends AbstractSendMessage implements TelegramMenuCommandInterface
{
    use TelegramUserSessionTrait;

    public ?User $user;

    /**
     * @param BotMenuCallbackTypeEnum $type
     * @param int $chatId
     *
     * @return void
     */
    public function execute(BotMenuCallbackTypeEnum $type, int $chatId): void
    {
        $this->chatId = $chatId;
        $userSession = $this->getLastSession($chatId, BotMenuCallbackTypeEnum::PRODUCT_CATEGORY->value);

        if ($userSession) {
            $user = $this->user;

            if (!$user) {
                return;
            }

            $agent = VisitPlan::whereTradePointCode($user->code)
                ->whereTradingAgentCode($userSession->message)->first();
            if ($agent) {
                $this->sendMessageWithOptions(trans('telegram.review_visit_plan',
                    ['name' => $agent->fio, 'date' => $agent->getNextVisitDate()]),
                    BotMenuMessage::toMainMenu()
                );

                return;
            }
        }

        $this->sendMessageWithOptions(trans('telegram.categories_null'), BotMenuMessage::toMainMenu());
    }

    /**
     * @param ?User $user
     *
     * @return $this
     */
    public function setUser(?User $user): static
    {
        $this->user = $user;

        return $this;
    }
}
