<?php

namespace App\Domain\TelegramBot\Commands\Menu;

use App\Domain\ComindWork\Enums\TaskTypeEnum;
use App\Domain\Review\Actions\ReviewAction;
use App\Domain\TelegramBot\Actions\TelegramSendMessageAction;
use App\Domain\TelegramBot\Base\BotMenuMessage;
use App\Domain\TelegramBot\Commands\AbstractSendMessage;
use App\Domain\TelegramBot\Contracts\TelegramMenuCommandInterface;
use App\Domain\TelegramBot\Enums\BotMenuCallbackTypeEnum;
use App\Domain\TelegramBot\Enums\ResponseStatusEnum;
use App\Domain\TelegramBot\Traits\TelegramUserSessionTrait;
use App\Models\User;
use App\Models\VisitPlan;
use Telegram\Bot\Objects\Update as UpdateObject;

//TODO refactor.
class ReviewVisitCommand extends AbstractSendMessage implements TelegramMenuCommandInterface
{
    use TelegramUserSessionTrait;

    public ?User $user;
    private UpdateObject $update;

    public function __construct(
        TelegramSendMessageAction $telegramSendMessageAction,
        public readonly ReviewAction $reviewAction
    ) {
        parent::__construct($telegramSendMessageAction);
    }

    /**
     * @param BotMenuCallbackTypeEnum $type
     * @param int $chatId
     *
     * @return void
     */
    public function execute(BotMenuCallbackTypeEnum $type, int $chatId): void
    {
        $this->chatId = $chatId;
        $userSession = $this->getLastSession($chatId, BotMenuCallbackTypeEnum::PRODUCT_CATEGORY->value);

        if ($userSession) {
            $user = $this->user;
            $message = $this->update->getMessage();

            if (!$user) {
                return;
            }

            $agent = VisitPlan::whereTradePointCode($user->code)
                ->whereTradingAgentCode($userSession->message)->first();
            if ($agent) {
                $text = trans('telegram.review_last_visit',
                    ['name' => $agent->fio, 'date' => $agent->getLastVisitDate()]);

                $this->setSession([
                    'message'  => $message->text,
                    'chatId'   => $chatId,
                    'botType'  => $message->objectType(),
                    'response' => json_encode($message->getRawResponse()),
                    'status'   => ResponseStatusEnum::SUCCESS,
                    'type'     => BotMenuCallbackTypeEnum::REVIEW_VISIT,
                ]);

                $data = [
                    'sales_agent_code' => $agent->trading_agent_code,
                    'visit_date'       => $agent->getLastVisitDate('Y-m-d'),
                ];

                $review = $this->reviewAction->getReview($user, TaskTypeEnum::AGENT->value, $data);
                if ($message->text !== trans('menu.review_visit')) {
                    $this->sendMessage($text);
                    $info = trans('telegram.visit_details', [
                        'date' => $agent->getLastVisitDate(),
                        'agent_name' => $agent->fio,
                        'agent_phone' => $agent->phone,
                        'name' => $user->fio,
                        'phone' => $user->tel,
                    ]);

                    if (preg_match('/^([0-9])\*$/', $message->text, $matches)) {
                        $data['rating'] = $matches[1];
                        $data['c_estimate_value'] = $data['rating'] . '*';

                        if ($review && !$review->rating) {
                            $this->reviewAction->updateByReview($review, $data);

                            $user->clearTelegramSession();

                            $trans = trans('telegram.visit_after_rating', ['rating' => $data['rating']]);
                            $this->sendMessageWithOptions($trans, BotMenuMessage::mainMenu());
                            return;
                        } elseif (!$review) {
                            $data['description'] = $info;

                            $review = $this->reviewAction->createByMessage($user, TaskTypeEnum::AGENT->value, $data);

                            $trans = trans('telegram.visit_review_comment', ['rating' => $review->rating]);
                            $this->sendMessageWithOptions($trans, BotMenuMessage::toMainMenu());
                            return;
                        }
                    } else {
                        $data['comment'] = $message->text;
                        $data['description'] = $message->text . $info;

                        if ($review && !$review->comment) {
                            $this->reviewAction->updateByReview($review, $data);

                            $user->clearTelegramSession();

                            $trans = trans('telegram.visit_after_comment');
                            $this->sendMessageWithOptions($trans, BotMenuMessage::mainMenu());
                            return;
                        } elseif (!$review) {
                            $this->reviewAction->createByMessage($user, TaskTypeEnum::AGENT->value, $data);

                            $this->sendMessageWithOptions(trans('telegram.visit_review_rating'),
                                BotMenuMessage::ratingMenu());
                            return;
                        }
                    }

                    return;
                }


                if ($review) {
                    if (!$review->comment && $review->rating) {
                        $trans = trans('telegram.visit_review_comment', ['rating' => $review->rating]);

                        $this->sendMessage($text);
                        $this->sendMessageWithOptions($trans, BotMenuMessage::toMainMenu());
                    } elseif (!$review->rating) {
                        $this->sendMessage($text);
                        $this->sendMessageWithOptions(trans('telegram.visit_review_rating'),
                            BotMenuMessage::ratingMenu());
                    } else {
                        $this->sendMessage($text);

                        $exists = trans('telegram.visit_review_exists', [
                            'rating'  => $review->rating,
                            'comment' => $review->comment,
                        ]);
                        $this->sendMessageWithOptions($exists, BotMenuMessage::toMainMenu());
                    }
                } else {
                    $this->sendMessage($text);

                    $this->sendMessageWithOptions(trans('telegram.visit_review'), BotMenuMessage::ratingMenu());
                }
                return;
            }
        }

        $this->sendMessageWithOptions(trans('telegram.error'), BotMenuMessage::toMainMenu());
    }

    public function setUser(?User $user): static
    {
        $this->user = $user;

        return $this;
    }

    public function setUpdate(UpdateObject $update): static
    {
        $this->update = $update;

        return $this;
    }

}
