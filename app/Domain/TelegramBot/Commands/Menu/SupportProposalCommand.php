<?php

namespace App\Domain\TelegramBot\Commands\Menu;

use App\Domain\ComindWork\Enums\TaskTypeEnum;
use App\Domain\Review\Actions\ReviewAction;
use App\Domain\TelegramBot\Actions\TelegramSendMessageAction;
use App\Domain\TelegramBot\Base\BotMenuMessage;
use App\Domain\TelegramBot\Commands\AbstractSendMessage;
use App\Domain\TelegramBot\Contracts\TelegramMenuCommandInterface;
use App\Domain\TelegramBot\Enums\BotMenuCallbackTypeEnum;
use App\Domain\TelegramBot\Enums\ResponseStatusEnum;
use App\Domain\TelegramBot\Traits\TelegramUserSessionTrait;
use App\Models\User;
use Telegram\Bot\Objects\Update as UpdateObject;

class SupportProposalCommand extends AbstractSendMessage implements TelegramMenuCommandInterface
{
    use TelegramUserSessionTrait;

    public ?User $user;
    private UpdateObject $update;

    public function __construct(TelegramSendMessageAction $telegramSendMessageAction,
        public readonly ReviewAction $reviewAction)
    {
        parent::__construct($telegramSendMessageAction);
    }

    /**
     * @param BotMenuCallbackTypeEnum $type
     * @param int $chatId
     *
     * @return void
     */
    public function execute(BotMenuCallbackTypeEnum $type, int $chatId): void
    {
        $this->chatId = $chatId;
        $user = $this->user;
        $message = $this->update->getMessage();

        if (!$user) {
            return;
        }

        if ($message->text !== trans('menu.proposal')) {
            $info = trans('telegram.client_detail', [
                'name' => $user->fio,
                'phone' => $user->tel,
            ]);

            $data = [
                'comment' => $message->text,
                'description' => $message->text . $info
            ];

            $this->reviewAction->createByMessage($user, TaskTypeEnum::PROPOSAL->value, $data);

            $user->clearTelegramSession();

            $this->sendMessageWithOptions(trans('telegram.proposal_feedback'), BotMenuMessage::toMainMenu());
            return;
        }

        $this->setSession([
            'message'  => $message->text,
            'chatId'   => $chatId,
            'botType'  => $message->objectType(),
            'response' => json_encode($message->getRawResponse()),
            'status'   => ResponseStatusEnum::SUCCESS,
            'type'     => BotMenuCallbackTypeEnum::PROPOSAL,
        ]);

        $this->sendMessageWithOptions(trans('telegram.proposal'), BotMenuMessage::toMainMenu());
    }

    public function setUser(?User $user): static
    {
        $this->user = $user;

        return $this;
    }

    public function setUpdate(UpdateObject $update): static
    {
        $this->update = $update;

        return $this;
    }

}
