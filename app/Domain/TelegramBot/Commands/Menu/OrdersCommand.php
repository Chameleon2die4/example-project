<?php
declare(strict_types=1);

namespace App\Domain\TelegramBot\Commands\Menu;

use App\Domain\TelegramBot\Commands\AbstractSendMessage;
use App\Domain\TelegramBot\Contracts\TelegramMenuCommandInterface;
use App\Domain\TelegramBot\Enums\BotMenuCallbackTypeEnum;
use App\Models\User;

class OrdersCommand extends AbstractSendMessage implements TelegramMenuCommandInterface
{
    public ?User $user;

    /**
     * @param BotMenuCallbackTypeEnum $type
     * @param int $chatId
     *
     * @return void
     */
    public function execute(BotMenuCallbackTypeEnum $type, int $chatId): void
    {
        $this->chatId = $chatId;
        $user = $this->user;

        if (!$user) {
            return;
        }

        $text = trans('telegram.main_menu_text', ['name' => $user->name]);

        $this->sendMessageWithOptions($text, [
            'params' => [
                [ ['text' => trans('menu.orders_in_work')], ['text' => trans('menu.orders_last')] ],
                [ ['text' => trans('menu.main_menu')] ],
            ],
            'is_keyboard' => true,
            'is_inline' => false,
            'resize_keyboard' => true,
        ]);
    }

    /**
     * @param User|null $user
     * @return $this
     */
    public function setUser(?User $user): static
    {
        $this->user = $user;

        return $this;
    }
}
