<?php
declare(strict_types=1);

namespace App\Domain\TelegramBot\Commands\Menu;

use App\Domain\TelegramBot\Base\BotMenuMessage;
use App\Domain\TelegramBot\Commands\AbstractSendMessage;
use App\Domain\TelegramBot\Contracts\TelegramMenuCommandInterface;
use App\Domain\TelegramBot\Enums\BotMenuCallbackTypeEnum;
use App\Models\BonusPoint;
use App\Models\User;

class BonusCommand extends AbstractSendMessage implements TelegramMenuCommandInterface
{
    public ?User $user;

    /**
     * @param BotMenuCallbackTypeEnum $type
     * @param int $chatId
     *
     * @return void
     */
    public function execute(BotMenuCallbackTypeEnum $type, int $chatId): void
    {
        $this->chatId = $chatId;
        $user = $this->user;

        if (!$user) {
            return;
        }

        $bonuses = BonusPoint::whereCode($user->code)->get();
        if ($bonuses->count()) {
            foreach ($bonuses as $ind => $bonus) {
                $balance = number_format($bonus->balance, 2, '.', ' ');

                $text = trans('telegram.bonus_info', [
                    'program' => $bonus->program,
                    'balance' => $balance,
                ]);

                if ($ind < ($bonuses->count() - 1)) {
                    $this->sendMessage($text);
                } else {
                    $this->sendMessageWithOptions($text, BotMenuMessage::toMainMenu());
                }
            }
        } else {
            $this->sendMessageWithOptions(trans('telegram.bonus_info_error'), BotMenuMessage::toMainMenu());
        }
    }

    /**
     * @param ?User $user
     *
     * @return $this
     */
    public function setUser(?User $user): static
    {
        $this->user = $user;

        return $this;
    }
}
