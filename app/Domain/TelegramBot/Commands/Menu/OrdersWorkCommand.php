<?php

namespace App\Domain\TelegramBot\Commands\Menu;

use App\Domain\ClientOrder\Actions\ClientOrderAction;
use App\Domain\TelegramBot\Actions\TelegramSendMessageAction;
use App\Domain\TelegramBot\Base\BotMenuMessage;
use App\Domain\TelegramBot\Contracts\TelegramMenuCommandInterface;
use App\Domain\TelegramBot\Enums\BotMenuCallbackTypeEnum;
use App\Domain\TelegramBot\Enums\ResponseStatusEnum;
use App\Domain\TelegramBot\Traits\SendMessageTrait;
use App\Domain\TelegramBot\Traits\TelegramUserSessionTrait;
use App\Models\ClientOrder;
use App\Models\User;
use Telegram\Bot\Objects\Update as UpdateObject;

class OrdersWorkCommand implements TelegramMenuCommandInterface
{
    use SendMessageTrait, TelegramUserSessionTrait;

    public ?User $user;

    private UpdateObject $update;

    public function __construct(
        public readonly ClientOrderAction $clientOrderAction,
        public readonly TelegramSendMessageAction $telegramSendMessageAction,
    ) {
    }

    /**
     * @param BotMenuCallbackTypeEnum $type
     * @param int $chatId
     *
     * @return void
     */
    public function execute(BotMenuCallbackTypeEnum $type, int $chatId): void
    {
        $this->chatId = $chatId;
        $user = $this->user;
        $message = $this->update->getMessage();

        if (!$user) {
            return;
        }

        $order = ClientOrder::whereNumber($message->text)->first();
        $orders = ClientOrder::where('trade_point_code', $user->code)
            ->where('status', '!=',  ClientOrder::STATUS_CLOSED)->orderBy('date')->get();

        if ($orders) {
            if ($order || $orders->count() === 1) {
                $order = $order ?? $orders->first();
                $text = $this->clientOrderAction->getOrderMessage($order);

                $this->sendMessageWithOptions($text, BotMenuMessage::toMainMenu());
                return;
            } elseif ($orders->count() > 1) {
                $this->setSession([
                    'message' => $message->text,
                    'chatId' => $chatId,
                    'botType' => $message->objectType(),
                    'response' => json_encode($message->getRawResponse()),
                    'status' => ResponseStatusEnum::SUCCESS,
                    'type' => BotMenuCallbackTypeEnum::ORDERS_WORK,
                ]);

                $params = $orders->map(function (ClientOrder $item) {
                    return [['text' => $item->number]];
                })->all();
                $params[] = [['text' => trans('menu.main_menu')]];

                $this->sendMessageWithOptions(trans('telegram.select_order'), [
                    'params'          => $params,
                    'is_keyboard'     => true,
                    'is_inline'       => false,
                    'resize_keyboard' => true,
                ]);
                return;
            }
        }

        $this->sendMessageWithOptions(trans('telegram.no_orders_work'), BotMenuMessage::toMainMenu());
    }

    public function setUser(?User $user): static
    {
        $this->user = $user;

        return $this;
    }

    public function setUpdate(UpdateObject $update): static
    {
        $this->update = $update;

        return $this;
    }

}
