<?php
declare(strict_types=1);

namespace App\Domain\TelegramBot\Commands\Menu;

use App\Domain\TelegramBot\Commands\AbstractSendMessage;
use App\Domain\TelegramBot\Contracts\TelegramMenuCommandInterface;
use App\Domain\TelegramBot\Enums\BotMenuCallbackTypeEnum;

class ChangeLangCommand extends AbstractSendMessage implements TelegramMenuCommandInterface
{
    /**
     * @param BotMenuCallbackTypeEnum $type
     * @param int $chatId
     *
     * @return void
     */
    public function execute(BotMenuCallbackTypeEnum $type, int $chatId): void
    {
        $this->chatId = $chatId;

        $text = trans('telegram.start_message');
        $this->sendMessageWithOptions($text, [
            'params' => [
                [
                    ['text' => trans('menu.lang_uz'), 'callback_data' => BotMenuCallbackTypeEnum::LANG_UZ->value],
                    ['text' => trans('menu.lang_ru'), 'callback_data' => BotMenuCallbackTypeEnum::LANG_RU->value]
                ],
            ],
            'is_keyboard' => true,
            'is_inline' => false,
            'resize_keyboard' => true,
        ]);
    }
}
