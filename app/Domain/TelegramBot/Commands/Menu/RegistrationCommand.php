<?php
declare(strict_types=1);

namespace App\Domain\TelegramBot\Commands\Menu;

use App\Domain\TelegramBot\Commands\AbstractSendMessage;
use App\Domain\TelegramBot\Contracts\TelegramMenuCommandInterface;
use App\Domain\TelegramBot\Enums\BotMenuCallbackTypeEnum;
use App\Domain\TelegramBot\Enums\RegistrationStepsEnum;
use App\Domain\TelegramBot\Enums\ResponseStatusEnum;
use App\Domain\TelegramBot\Traits\TelegramUserSessionTrait;
use App\Models\TelegramUserSession;
use Throwable;

class RegistrationCommand extends AbstractSendMessage implements TelegramMenuCommandInterface
{
    use TelegramUserSessionTrait;

    private string|null $message;

    /**
     * @param BotMenuCallbackTypeEnum $type
     * @param int $chatId
     *
     * @return void
     * @throws Throwable
     */
    public function execute(BotMenuCallbackTypeEnum $type, int $chatId): void
    {
        $this->chatId = $chatId;
        $userSession = TelegramUserSession::query()->where('chat_id', $chatId)->latest()->first();

        if ($userSession && $userSession->type === RegistrationStepsEnum::PROVIDER_VALIDATION->value) {
            $text = trans('telegram.set_your_provider_error');
        } else {
            $this->setSession([
                'message' => $this->message,
                'chatId' => $chatId,
                'botType' => 'text',
                'response' => json_encode('validation'),
                'status' => ResponseStatusEnum::SUCCESS,
                'type' => RegistrationStepsEnum::PROVIDER_VALIDATION,
            ]);

            $text = trans('telegram.set_your_provider_id');
        }

        $this->sendMessage($text);

    }

    /**
     * @param string|null $message
     *
     * @return $this
     */
    public function setMessage(?string $message): static
    {
        $this->message = $message;

        return $this;
    }
}
