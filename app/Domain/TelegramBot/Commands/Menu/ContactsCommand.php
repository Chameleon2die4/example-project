<?php
declare(strict_types=1);

namespace App\Domain\TelegramBot\Commands\Menu;

use App\Domain\TelegramBot\Base\BotMenuMessage;
use App\Domain\TelegramBot\Commands\AbstractSendMessage;
use App\Domain\TelegramBot\Contracts\TelegramMenuCommandInterface;
use App\Domain\TelegramBot\Enums\BotMenuCallbackTypeEnum;
use App\Domain\TelegramBot\Traits\TelegramUserSessionTrait;
use App\Models\User;
use App\Models\VisitPlan;

class ContactsCommand extends AbstractSendMessage implements TelegramMenuCommandInterface
{
    use TelegramUserSessionTrait;

    public ?User $user;

    /**
     * @param BotMenuCallbackTypeEnum $type
     * @param int $chatId
     *
     * @return void
     */
    public function execute(BotMenuCallbackTypeEnum $type, int $chatId): void
    {
        $this->chatId = $chatId;

        $userSession = $this->getLastSession($chatId, BotMenuCallbackTypeEnum::PRODUCT_CATEGORY->value);
        if ($userSession) {
            $user = $this->user;

            $agent = VisitPlan::whereTradePointCode($user->code)
                ->whereTradingAgentCode($userSession->message)->first();
            if ($agent) {
                $this->sendMessageWithOptions(trans('telegram.contact_info',
                    ['name' => $agent->fio, 'phone' => $agent->phone]), BotMenuMessage::toMainMenu());

                return;
            }
        }

        $this->sendMessageWithOptions(trans('telegram.categories_null'), BotMenuMessage::toMainMenu());
    }

    public function setUser(?User $user): static
    {
        $this->user = $user;

        return $this;
    }
}

