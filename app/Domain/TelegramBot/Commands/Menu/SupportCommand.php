<?php
declare(strict_types=1);

namespace App\Domain\TelegramBot\Commands\Menu;

use App\Domain\TelegramBot\Commands\AbstractSendMessage;
use App\Domain\TelegramBot\Contracts\TelegramMenuCommandInterface;
use App\Domain\TelegramBot\Enums\BotMenuCallbackTypeEnum;

class SupportCommand extends AbstractSendMessage implements TelegramMenuCommandInterface
{
    /**
     * @param BotMenuCallbackTypeEnum $type
     * @param int $chatId
     *
     * @return void
     */
    public function execute(BotMenuCallbackTypeEnum $type, int $chatId): void
    {
        $this->chatId = $chatId;

        $this->sendMessageWithOptions(trans('telegram.support_select'), [
            'params'          => [
                [['text' => trans('menu.complaint')], ['text' => trans('menu.proposal')]],
                [['text' => trans('menu.main_menu')]],
            ],
            'is_keyboard'     => true,
            'is_inline'       => false,
            'resize_keyboard' => true,
        ]);
    }

}
