<?php

namespace App\Domain\TelegramBot\Commands\Menu;

use App\Domain\TelegramBot\Base\BotMenuMessage;
use App\Domain\TelegramBot\Commands\AbstractSendMessage;
use App\Domain\TelegramBot\Contracts\TelegramMenuCommandInterface;
use App\Domain\TelegramBot\Enums\BotMenuCallbackTypeEnum;
use App\Models\Balance;
use App\Models\User;

class BalanceCommand extends AbstractSendMessage implements TelegramMenuCommandInterface
{
    public ?User $user;

    /**
     * @param BotMenuCallbackTypeEnum $type
     * @param int $chatId
     *
     * @return void
     */
    public function execute(BotMenuCallbackTypeEnum $type, int $chatId): void
    {
        $this->chatId = $chatId;
        $user = $this->user;

        if (!$user) {
            return;
        }

        $balance = Balance::wherePointCode($user->code)->first();
        if ($balance) {
            $debit = number_format($balance->debit, 2, '.', ' ');
            $shipment_amount = number_format($balance->shipment_amount, 2, '.', ' ');

            $text = trans('telegram.balance_info', [
                'debit'           => $debit,
                'shipment_amount' => $shipment_amount,
            ]);

            $this->sendMessageWithOptions($text, BotMenuMessage::toMainMenu());
        } else {
            $this->sendMessageWithOptions(trans('telegram.balance_error'), BotMenuMessage::toMainMenu());
        }
    }

    /**
     * @param ?User $user
     *
     * @return $this
     */
    public function setUser(?User $user): static
    {
        $this->user = $user;

        return $this;
    }

}
