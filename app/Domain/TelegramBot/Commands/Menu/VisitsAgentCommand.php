<?php
declare(strict_types=1);

namespace App\Domain\TelegramBot\Commands\Menu;

use App\Domain\TelegramBot\Base\BotMenuMessage;
use App\Domain\TelegramBot\Commands\AbstractSendMessage;
use App\Domain\TelegramBot\Contracts\TelegramMenuCommandInterface;
use App\Domain\TelegramBot\Enums\BotMenuCallbackTypeEnum;
use App\Domain\TelegramBot\Enums\ResponseStatusEnum;
use App\Domain\TelegramBot\Traits\TelegramUserSessionTrait;
use App\Models\ProductCategory;
use App\Models\User;
use Telegram\Bot\Objects\Update as UpdateObject;

class VisitsAgentCommand extends AbstractSendMessage implements TelegramMenuCommandInterface
{
    use TelegramUserSessionTrait;

    public ?User $user;

    private UpdateObject $update;

    /**
     * @param BotMenuCallbackTypeEnum $type
     * @param int $chatId
     *
     * @return void
     */
    public function execute(BotMenuCallbackTypeEnum $type, int $chatId): void
    {
        $this->chatId = $chatId;
        $user = $this->user;

        if (!$user) {
            return;
        }

        $message = $this->update->getMessage();

        $category = ProductCategory::whereTradePointCode($user->code)
            ->whereName($message->text)->first();
        if ($category) {
            $this->removeSessionByType($chatId, BotMenuCallbackTypeEnum::VISITS_AGENT->value);

            $this->setSession([
                'message' => $category->trading_agent_code,
                'chatId' => $chatId,
                'botType' => $message->objectType(),
                'response' => json_encode($message->getRawResponse()),
                'status' => ResponseStatusEnum::SUCCESS,
                'type' => BotMenuCallbackTypeEnum::PRODUCT_CATEGORY,
            ]);

            $this->sendMessageWithOptions(trans('telegram.selected_category', ['name' => $category->full_name]), [
                'params' => [
                    [['text' => trans('menu.visit_plan')], ['text' => trans('menu.contacts')]],
                    [['text' => trans('menu.review_visit')]],
                    [['text' => trans('menu.main_menu')]],
                ],
                'is_keyboard' => true,
                'is_inline' => false,
                'resize_keyboard' => true,
            ]);

            return;
        }

        $this->setSession([
            'message' => $message->text ?? '',
            'chatId' => $chatId,
            'botType' => $message->objectType(),
            'response' => json_encode($message->getRawResponse()),
            'status' => ResponseStatusEnum::SUCCESS,
            'type' => BotMenuCallbackTypeEnum::VISITS_AGENT,
        ]);

        $categories = ProductCategory::where('trade_point_code', $user->code)
            ->pluck('name')->unique()->values();
        if ($categories->count()) {
            $params = $categories->map(function ($item) {
                return [['text' => $item]];
            })->all();
            $params[] = [['text' => trans('menu.main_menu')]];

            $text = trans('telegram.choose_category');
            $this->sendMessageWithOptions($text, [
                'params'          => $params,
                'is_keyboard'     => true,
                'is_inline'       => false,
                'resize_keyboard' => true,
            ]);
        } else {
            $this->sendMessageWithOptions(trans('telegram.categories_null'), BotMenuMessage::toMainMenu());
        }
    }

    /**
     * @param User|null $user
     *
     * @return $this
     */
    public function setUser(?User $user): static
    {
        $this->user = $user;

        return $this;
    }

    public function setUpdate(UpdateObject $update): static
    {
        $this->update = $update;

        return $this;
    }
}
