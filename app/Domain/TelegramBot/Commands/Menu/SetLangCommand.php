<?php
declare(strict_types=1);

namespace App\Domain\TelegramBot\Commands\Menu;

use App\Domain\TelegramBot\Commands\AbstractSendMessage;
use App\Domain\TelegramBot\Contracts\TelegramMenuCommandInterface;
use App\Domain\TelegramBot\Enums\BotMenuCallbackTypeEnum;
use App\Domain\TelegramBot\Enums\ResponseStatusEnum;
use App\Domain\TelegramBot\Traits\TelegramUserSessionTrait;
use App\Models\User;
use BotLanguage;
use Telegram\Bot\Objects\Update as UpdateObject;

class SetLangCommand extends AbstractSendMessage implements TelegramMenuCommandInterface
{
    use TelegramUserSessionTrait;

    public ?User $user;
    private UpdateObject $update;

    /**
     * @param BotMenuCallbackTypeEnum $type
     * @param int $chatId
     *
     * @return void
     * @noinspection PhpPossiblePolymorphicInvocationInspection
     */
    public function execute(BotMenuCallbackTypeEnum $type, int $chatId): void
    {
        $this->chatId = $chatId;
        $message = $this->update->getMessage();

        $lang = $this->getLanguage($message->text);

        $this->removeSessionByType($chatId, BotMenuCallbackTypeEnum::CHANGE_LANG->value);
        $this->setSession([
            'message' => $lang,
            'chatId' => $chatId,
            'botType' => $message->objectType(),
            'response' => json_encode($message->getRawResponse()),
            'status' => ResponseStatusEnum::SUCCESS,
            'type' => BotMenuCallbackTypeEnum::CHANGE_LANG,
        ]);

        BotLanguage::set($lang);

        $this->sendMessageWithOptions(trans('telegram.select'), [
            'params' => [
                [
                    ['text' => trans('menu.registration'), 'callback_data' => BotMenuCallbackTypeEnum::REGISTRATION->value],
                ],
                [
                    ['text' => trans('menu.change_lang'), 'callback_data' => BotMenuCallbackTypeEnum::CHANGE_LANG->value],
                ],
                [
                    ['text' => trans('menu.about_bot'), 'callback_data' => BotMenuCallbackTypeEnum::ABOUT_BOT->value],
                ],
            ],
            'is_keyboard' => true,
            'is_inline' => false,
            'resize_keyboard' => true,
        ]);
    }

    public function getLanguage($text) {
        return match ($text) {
            trans('menu.lang_uz') => 'uz',
            default => 'ru',
        };
    }

    public function setUser(?User $user): static
    {
        $this->user = $user;

        return $this;
    }

    public function setUpdate(UpdateObject $update): static
    {
        $this->update = $update;

        return $this;
    }

}
