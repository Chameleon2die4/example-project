<?php

namespace App\Domain\TelegramBot\Commands\Menu;

use App\Domain\ClientOrder\Actions\ClientOrderAction;
use App\Domain\ComindWork\Enums\TaskTypeEnum;
use App\Domain\Review\Actions\ReviewAction;
use App\Domain\TelegramBot\Actions\TelegramSendMessageAction;
use App\Domain\TelegramBot\Base\BotMenuMessage;
use App\Domain\TelegramBot\Contracts\TelegramMenuCommandInterface;
use App\Domain\TelegramBot\Enums\BotMenuCallbackTypeEnum;
use App\Domain\TelegramBot\Enums\ResponseStatusEnum;
use App\Domain\TelegramBot\Traits\SendMessageTrait;
use App\Domain\TelegramBot\Traits\TelegramUserSessionTrait;
use App\Models\ClientOrder;
use App\Models\User;
use Telegram\Bot\Objects\Update as UpdateObject;

class OrderLastCommand implements TelegramMenuCommandInterface
{
    use SendMessageTrait, TelegramUserSessionTrait;

    public ?User $user;
    private UpdateObject $update;
    private string $menu;

    public function __construct(
        public readonly ClientOrderAction $clientOrderAction,
        public readonly ReviewAction $reviewAction,
        public readonly TelegramSendMessageAction $telegramSendMessageAction,
    ) {
        $this->menu = trans('menu.orders_last');
    }

    /**
     * @param BotMenuCallbackTypeEnum $type
     * @param int $chatId
     *
     * @return void
     * @noinspection PhpPossiblePolymorphicInvocationInspection
     */
    public function execute(BotMenuCallbackTypeEnum $type, int $chatId): void
    {
        $this->chatId = $chatId;
        $user = $this->user;

        if (!$user) {
            return;
        }

        $order = ClientOrder::where('trade_point_code', $user->code)
            ->where('status', ClientOrder::STATUS_CLOSED)->orderBy('date')->first();

        if ($order) {
            $text = $this->clientOrderAction->getOrderMessage($order);

            $message = $this->update->getMessage();

            $this->setSession([
                'message'  => $message->text,
                'chatId'   => $chatId,
                'botType'  => $message->objectType(),
                'response' => json_encode($message->getRawResponse()),
                'status'   => ResponseStatusEnum::SUCCESS,
                'type'     => BotMenuCallbackTypeEnum::ORDERS_LAST,
            ]);

            $data = [
                'order_code' => $order->number,
                'c_order'    => $order->name,
            ];

            $review = $this->reviewAction->getReview($user, TaskTypeEnum::ORDER->value);
            if ($message->text !== $this->menu) {
                $this->sendMessage($text);
                $info = trans('telegram.client_detail', [
                    'name' => $user->fio,
                    'phone' => $user->tel,
                ]);

                if (preg_match('/^([0-9])\*$/', $message->text, $matches)) {
                    $data['rating'] = $matches[1];
                    $data['c_estimate_value'] = $data['rating'] . '*';

                    if ($review && !$review->rating) {
                        $this->reviewAction->updateByReview($review, $data);

                        $user->clearTelegramSession();

                        $trans = trans('telegram.order_after_rating', ['rating' => $data['rating']]);
                        $this->sendMessageWithOptions($trans, BotMenuMessage::mainMenu());
                        return;
                    } elseif (!$review) {
                        $data['description'] = $info;

                        $review = $this->reviewAction->createByMessage($user, TaskTypeEnum::ORDER->value, $data);

                        $trans = trans('telegram.order_review_comment', ['rating' => $review->rating]);
                        $this->sendMessageWithOptions($trans, BotMenuMessage::toMainMenu());
                        return;
                    }
                } else {
                    $data['comment'] = $message->text;
                    $data['description'] = $message->text . $info;

                    if ($review && !$review->comment) {
                        $this->reviewAction->updateByReview($review, $data);

                        $user->clearTelegramSession();

                        $trans = trans('telegram.order_after_comment');
                        $this->sendMessageWithOptions($trans, BotMenuMessage::mainMenu());
                        return;
                    } elseif (!$review) {
                        $this->reviewAction->createByMessage($user, TaskTypeEnum::ORDER->value, $data);

                        $this->sendMessageWithOptions(trans('telegram.order_review_rating'),
                            BotMenuMessage::ratingMenu());
                        return;
                    }
                }

                return;
            }


            if ($review) {
                if (!$review->comment && $review->rating) {
                    $message = trans('telegram.order_review_comment', ['rating' => $review->rating]);

                    $this->sendMessage($text);
                    $this->sendMessageWithOptions($message, BotMenuMessage::toMainMenu());
                } elseif (!$review->rating) {
                    $this->sendMessage($text);
                    $this->sendMessageWithOptions(trans('telegram.order_review_rating'), BotMenuMessage::ratingMenu());
                } else {
                    $this->sendMessageWithOptions($text, BotMenuMessage::toMainMenu());
                }
            } else {
                $this->sendMessage($text);

                $this->sendMessageWithOptions(trans('telegram.order_review'), BotMenuMessage::ratingMenu());
            }
            return;
        }

        $this->sendMessageWithOptions(trans('telegram.no_orders_old'), BotMenuMessage::toMainMenu());
    }

    public function setUser(?User $user): static
    {
        $this->user = $user;

        return $this;
    }

    public function setUpdate(UpdateObject $update): static
    {
        $this->update = $update;

        return $this;
    }

}
