<?php
declare(strict_types=1);

namespace App\Domain\TelegramBot\Commands\Menu;

use App\Domain\TelegramBot\Commands\AbstractSendMessage;
use App\Domain\TelegramBot\Contracts\TelegramMenuCommandInterface;
use App\Domain\TelegramBot\Enums\BotMenuCallbackTypeEnum;

class AboutBotCommand extends AbstractSendMessage implements TelegramMenuCommandInterface
{
    /**
     * @param BotMenuCallbackTypeEnum $type
     * @param int $chatId
     *
     * @return void
     */
    public function execute(BotMenuCallbackTypeEnum $type, int $chatId): void
    {
        $this->chatId = $chatId;

        $this->sendMessage(trans('telegram.welcome'));
    }
}
