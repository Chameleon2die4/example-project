<?php
declare(strict_types=1);

namespace App\Domain\TelegramBot\Commands\Menu\Promo;

use App\Domain\SpecialProposition\Actions\SpecialPropositionAction;
use App\Domain\TelegramBot\Actions\TelegramSendMessageAction;
use App\Domain\TelegramBot\Base\BotMenuMessage;
use App\Domain\TelegramBot\Contracts\TelegramMenuCommandInterface;
use App\Domain\TelegramBot\Enums\BotMenuCallbackTypeEnum;
use App\Domain\TelegramBot\Traits\SendMessageTrait;
use App\Domain\TelegramBot\Traits\TelegramUserSessionTrait;
use App\Models\PromoBrand;
use App\Models\User;
use Illuminate\Support\Carbon;
use Telegram\Bot\Objects\Update as UpdateObject;

class SpecialPropositionCommand implements TelegramMenuCommandInterface
{
    use TelegramUserSessionTrait, SendMessageTrait;

    public ?User $user;
    private UpdateObject $update;
    private string $format = 'd.m.Y';

    private TelegramSendMessageAction $telegramSendMessageAction;
    private SpecialPropositionAction $specialPropositionAction;

    public function __construct(
        TelegramSendMessageAction $telegramSendMessageAction,
        SpecialPropositionAction  $specialPropositionAction
    )
    {
        $this->telegramSendMessageAction = $telegramSendMessageAction;
        $this->specialPropositionAction = $specialPropositionAction;
    }

    /**
     * @param BotMenuCallbackTypeEnum $type
     * @param int $chatId
     *
     * @return void
     */
    public function execute(BotMenuCallbackTypeEnum $type, int $chatId): void
    {
        $this->chatId = $chatId;
        $user = $this->user;
        $message = $this->update->getMessage();

        if (!$user) {
            return;
        }

        $query = $this->specialPropositionAction->getQueryByUser($user);
        $brand = PromoBrand::where('name', $message->text)->first();

        if ($query !== null && $brand) {
            $propositions = $query->where('brand_code', $brand->code)->get();
            foreach ($propositions as $key => $proposition) {
                $text = trans('telegram.proposition',
                    [
                        'name' => $proposition->text,
                        'description' => $proposition->details,
                        'start' => Carbon::parse($proposition->start_at)->format($this->format),
                        'end' => Carbon::parse($proposition->end_at)->format($this->format),
                    ]);

                if ($key < ($propositions->count() - 1)) {
                    $this->sendMessage($text);
                } else {
                    $this->sendMessageWithOptions($text, BotMenuMessage::toMainMenu());
                }
            }

            return;
        }

        $this->sendMessageWithOptions(trans('telegram.proposition_error'), BotMenuMessage::toMainMenu());
    }

    public function setUser(?User $user): static
    {
        $this->user = $user;

        return $this;
    }

    public function setUpdate(UpdateObject $update): static
    {
        $this->update = $update;

        return $this;
    }
}
