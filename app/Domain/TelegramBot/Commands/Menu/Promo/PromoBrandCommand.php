<?php

namespace App\Domain\TelegramBot\Commands\Menu\Promo;

use App;
use App\Domain\SpecialProposition\Actions\SpecialPropositionAction;
use App\Domain\TelegramBot\Actions\TelegramSendMessageAction;
use App\Domain\TelegramBot\Base\BotMenuMessage;
use App\Domain\TelegramBot\Enums\BotMenuCallbackTypeEnum;
use App\Domain\TelegramBot\Enums\ResponseStatusEnum;
use App\Domain\TelegramBot\Traits\SendMessageTrait;
use App\Domain\TelegramBot\Traits\TelegramUserSessionTrait;
use App\Models\PromoCategory;
use App\Models\User;
use Telegram\Bot\Objects\Update as UpdateObject;

class PromoBrandCommand
{
    use TelegramUserSessionTrait, SendMessageTrait;

    public ?User $user;
    private UpdateObject $update;
    private string $format;
    private SpecialPropositionAction $specialPropositionAction;
    private TelegramSendMessageAction $telegramSendMessageAction;

    public function __construct(
        TelegramSendMessageAction $telegramSendMessageAction,
        SpecialPropositionAction $specialPropositionAction
    ) {
        $this->telegramSendMessageAction = $telegramSendMessageAction;
        $this->specialPropositionAction = $specialPropositionAction;
    }

    /**
     * @param BotMenuCallbackTypeEnum $type
     * @param int $chatId
     *
     * @return void
     */
    public function execute(BotMenuCallbackTypeEnum $type, int $chatId): void
    {
        $this->chatId = $chatId;
        $user = $this->user;
        $message = $this->update->getMessage();
        $brands = null;

        $this->setSession([
            'message'  => $message->text,
            'chatId'   => $chatId,
            'botType'  => $message->objectType(),
            'response' => json_encode($message->getRawResponse()),
            'status'   => ResponseStatusEnum::SUCCESS,
            'type'     => BotMenuCallbackTypeEnum::SPECIAL_PROPOSITION,
        ]);

        $propositions = $this->specialPropositionAction->getQueryByUser($user);

        $locale = App::currentLocale();
        $category = PromoCategory::where("name->{$locale}", $message->text)->first();

        if ($propositions !== null && $category) {
            $propositions->where('category_code', $category->code);
            $brands = $this->specialPropositionAction->getBrands($propositions);
        }

        if ($brands && $brands->count()) {
            $params = $brands->map(function ($item) {
                return [['text' => $item->name]];
            })->all();
            $params[] = [['text' => trans('menu.main_menu')]];

            $this->sendMessageWithOptions(trans('telegram.select_promo_brand'), [
                'params'          => $params,
                'is_keyboard'     => true,
                'is_inline'       => false,
                'resize_keyboard' => true,
            ]);
        } else {
            $this->sendMessageWithOptions(trans('telegram.proposition_error'), BotMenuMessage::toMainMenu());
        }
    }

    public function setUser(?User $user): static
    {
        $this->user = $user;

        return $this;
    }

    public function setUpdate(UpdateObject $update): static
    {
        $this->update = $update;

        return $this;
    }
}
