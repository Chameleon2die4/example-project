<?php

namespace App\Domain\TelegramBot\Console\Commands;

use App\Domain\TelegramBot\Actions\Log\TelegramNotificationLog;
use App\Domain\TelegramBot\Commands\Special\NotificationCommand;
use App\Jobs\TelegramSendMessage;
use App\Models\Notice;
use App\Models\NoticeTradingPoint;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Console\Command\Command as BaseCommand;
use Throwable;

class SendNotificationsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:notifications';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Notifications';

    public function __construct(
        private readonly NotificationCommand $notificationCommand,
        private readonly TelegramNotificationLog $telegramNotificationLog
    ) {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     * @throws Throwable
     */
    public function handle()
    {
        $this->info('Send Notifications start...');

        $notices = Notice::where('for_all', 0)->get();
        foreach ($notices as $notice) {
            $codes = NoticeTradingPoint::where('notice_code', $notice->code)
                ->pluck('trading_point_code')->unique()->toArray();
            $codes_str = implode('","', $codes);

            $users = User::whereDoesntHave('history_notices',
                function (Builder $q) use ($notice) {
                    $q->where('notice_code', $notice->code);
                })->whereRaw('code IN ("'.$codes_str.'")')
                ->whereNotNull('chat_id')
                ->where('status', 1)
                ->where('verify', 1)
                ->where('is_inactive', 0)
                ->get();

            if ($users->count()) {
                foreach ($users as $user) {
                    $this->telegramNotificationLog
                        ->setChatId((int)$user->chat_id)
                        ->setTradingPointCode($user->code)
                        ->setNoticeCode($notice->code)
                        ->setMessage($notice->text)
                        ->setType('special')
                        ->execute();

                    $this->notificationCommand->execute($notice->text,
                        $user->chat_id, $user);
                }
            }

            $notice->done = 1;
            $notice->save();
        }


        $noticeList = Notice::where('for_all', 1)->get();

        foreach ($noticeList as $notice) {
            $this->allUsersMessage($notice);
            $notice->done = 1;
            $notice->save();
        }

        $this->clearNotice();

        $this->info('Send Notifications finished!');

        return BaseCommand::SUCCESS;
    }

    /**
     * @param  Notice  $notice
     *
     * @return void
     * @throws Throwable
     */
    private function allUsersMessage(Notice $notice): void
    {
        $users = User::whereNotNull('chat_id')
            ->where('status', 1)
            ->where('verify', 1)
            ->where('is_inactive', 0)
            ->get();

        $events = [];
        foreach ($users as $user) {
            if ($user instanceof User) {
                $events[] =
                    new TelegramSendMessage($notice->text, $user, $notice);
            }
        }

        Bus::batch($events)->name('TelegramSendMessage')->onQueue('send')
            ->dispatch();
    }

    /**
     * @return void
     * @throws Throwable
     */
    private function clearNotice(): void
    {
        DB::beginTransaction();
        $noticeList = Notice::where('done', 1)->get();

        foreach ($noticeList as $notice) {
            /** @var Notice $notice */
            NoticeTradingPoint::where('notice_code', $notice->code)->delete();
            $notice->delete();
        }

        DB::commit();
    }
}
