<?php
declare(strict_types=1);

namespace App\Domain\TelegramBot\Base;

class BotMenuMessage
{
    public static function toMainMenu(): array
    {
        return [
            'params' => [[['text' => trans('menu.main_menu')]]],
            'is_keyboard' => true,
            'is_inline' => false,
            'resize_keyboard' => true,
        ];
    }

    public static function mainMenu() {
        return [
            'params' => [
                [ ['text' => trans('menu.visits_agent')], ['text' => trans('menu.bonus')] ],
                [ ['text' => trans('menu.orders')], ['text' => trans('menu.special_proposition')] ],
                [ ['text' => trans('menu.balance')] ],
                [ ['text' => trans('menu.support')] ],
            ],
            'is_keyboard' => true,
            'is_inline' => false,
            'resize_keyboard' => true,
        ];
    }
    public static function ratingMenu(): array
    {
        return [
            'params' => [
                [['text' => '1*'], ['text' => '2*'], ['text' => '3*'], ['text' => '4*'], ['text' => '5*']],
                [['text' => trans('menu.main_menu')]],
            ],
            'is_keyboard' => true,
            'is_inline' => false,
            'resize_keyboard' => true,
        ];
    }

}
