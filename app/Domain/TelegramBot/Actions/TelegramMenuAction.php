<?php
declare(strict_types=1);

namespace App\Domain\TelegramBot\Actions;

use App\Domain\TelegramBot\Commands\Menu\AboutBotCommand;
use App\Domain\TelegramBot\Commands\Menu\BalanceCommand;
use App\Domain\TelegramBot\Commands\Menu\BonusCommand;
use App\Domain\TelegramBot\Commands\Menu\ChangeLangCommand;
use App\Domain\TelegramBot\Commands\Menu\ContactsCommand;
use App\Domain\TelegramBot\Commands\Menu\MainMenuCommand;
use App\Domain\TelegramBot\Commands\Menu\OrdersCommand;
use App\Domain\TelegramBot\Commands\Menu\OrderLastCommand;
use App\Domain\TelegramBot\Commands\Menu\OrdersWorkCommand;
use App\Domain\TelegramBot\Commands\Menu\Promo\PromoBrandCommand;
use App\Domain\TelegramBot\Commands\Menu\Promo\PromoCategoryCommand;
use App\Domain\TelegramBot\Commands\Menu\Promo\SpecialPropositionCommand;
use App\Domain\TelegramBot\Commands\Menu\RegistrationCommand;
use App\Domain\TelegramBot\Commands\Menu\ReviewVisitCommand;
use App\Domain\TelegramBot\Commands\Menu\SetLangCommand;
use App\Domain\TelegramBot\Commands\Menu\SupportCommand;
use App\Domain\TelegramBot\Commands\Menu\SupportComplaintCommand;
use App\Domain\TelegramBot\Commands\Menu\SupportProposalCommand;
use App\Domain\TelegramBot\Commands\Menu\VisitPlanCommand;
use App\Domain\TelegramBot\Commands\Menu\VisitsAgentCommand;
use App\Domain\TelegramBot\Enums\BotMenuCallbackTypeEnum;
use App\Domain\TelegramBot\Events\TelegramUserActivityLogEvent;
use App\Models\User;
use Telegram\Bot\Objects\Update as UpdateObject;
use Throwable;

class TelegramMenuAction
{
    private User|null $user;

    private UpdateObject $update;

    /**
     * @param ChangeLangCommand $changeLangCommand
     * @param RegistrationCommand $registrationCommand
     * @param BonusCommand $bonusCommand
     * @param MainMenuCommand $mainMenuCommand
     * @param OrdersCommand $ordersCommand
     * @param SpecialPropositionCommand $specialPropositionCommand
     * @param SupportCommand $supportCommand
     * @param AboutBotCommand $aboutBotCommand
     * @param SetLangCommand $setLangCommand
     * @param VisitsAgentCommand $visitsAgentCommand
     * @param VisitPlanCommand $visitPlanCommand
     * @param ContactsCommand $contactsCommand
     * @param OrderLastCommand $ordersOldCommand
     * @param OrdersWorkCommand $ordersWorkCommand
     * @param PromoCategoryCommand $promoCategoryCommand
     * @param PromoBrandCommand $promoBrandCommand
     * @param BalanceCommand $balanceCommand
     * @param SupportProposalCommand $proposalCommand
     * @param SupportComplaintCommand $complaintCommand
     * @param ReviewVisitCommand $reviewVisitCommand
     */
    public function __construct(
        private readonly ChangeLangCommand $changeLangCommand,
        private readonly RegistrationCommand $registrationCommand,
        private readonly BonusCommand $bonusCommand,
        private readonly MainMenuCommand $mainMenuCommand,
        private readonly OrdersCommand $ordersCommand,
        private readonly SpecialPropositionCommand $specialPropositionCommand,
        private readonly SupportCommand $supportCommand,
        private readonly AboutBotCommand $aboutBotCommand,
        private readonly SetLangCommand $setLangCommand,
        private readonly VisitsAgentCommand $visitsAgentCommand,
        private readonly VisitPlanCommand $visitPlanCommand,
        private readonly ContactsCommand $contactsCommand,
        private readonly OrderLastCommand $ordersOldCommand,
        private readonly OrdersWorkCommand $ordersWorkCommand,
        private readonly PromoCategoryCommand $promoCategoryCommand,
        private readonly PromoBrandCommand $promoBrandCommand,
        private readonly BalanceCommand $balanceCommand,
        private readonly SupportProposalCommand $proposalCommand,
        private readonly SupportComplaintCommand $complaintCommand,
        private readonly ReviewVisitCommand $reviewVisitCommand,
    ) {
    }

    /**
     * @param BotMenuCallbackTypeEnum $callbackType
     *
     * @return void
     * @throws Throwable
     */
    public function execute(BotMenuCallbackTypeEnum $callbackType): void
    {
        $chat = $this->update->getChat();
        $message = $this->update->getMessage();
        $user = $this->user;

        event(new TelegramUserActivityLogEvent([
            'user_id' => $this->user?->id,
            'chat_id' => $chat->id,
            'response' => json_encode($message->getRawResponse()),
            'step' => $callbackType->value,
            'message' => $message->text,
        ]));

        switch ($callbackType) {
            case BotMenuCallbackTypeEnum::CHANGE_LANG:
                $this->changeLangCommand->execute($callbackType, $chat->id);
                break;
            case BotMenuCallbackTypeEnum::LANG_RU:
            case BotMenuCallbackTypeEnum::LANG_UZ:
                $this->setLangCommand
                    ->setUser($user)
                    ->setUpdate($this->update)
                    ->execute($callbackType, $chat->id);
                break;

            case BotMenuCallbackTypeEnum::REGISTRATION:
                $this->registrationCommand
                    ->setMessage($message->text)
                    ->execute($callbackType, $chat->id);
                break;
            case BotMenuCallbackTypeEnum::ABOUT_BOT:
                $this->aboutBotCommand->execute($callbackType, $chat->id);
                break;
            case BotMenuCallbackTypeEnum::BONUS:
                $this->bonusCommand->setUser($user)
                    ->execute($callbackType, $chat->id);
                break;
            case BotMenuCallbackTypeEnum::ORDERS:
                $this->ordersCommand->setUser($user)
                    ->execute($callbackType, $chat->id);
                break;
            case BotMenuCallbackTypeEnum::SPECIAL_PROPOSITION:
                $this->specialPropositionCommand->setUser($user)
                    ->setUpdate($this->update)
                    ->execute($callbackType, $chat->id);
                break;
            case BotMenuCallbackTypeEnum::SUPPORT:
                $this->supportCommand->execute($callbackType, $chat->id);
                break;
            case BotMenuCallbackTypeEnum::MAIN_MENU:
                $this->mainMenuCommand->setUser($user)
                    ->execute($callbackType, $chat->id);
                break;
            case BotMenuCallbackTypeEnum::ORDERS_LAST:
                $this->ordersOldCommand->setUser($user)
                    ->setUpdate($this->update)
                    ->execute($callbackType, $chat->id);
                break;
            case BotMenuCallbackTypeEnum::ORDERS_WORK:
                $this->ordersWorkCommand->setUser($user)
                    ->setUpdate($this->update)
                    ->execute($callbackType, $chat->id);
                break;
            case BotMenuCallbackTypeEnum::PROMO_CATEGORY:
                $this->promoCategoryCommand->setUser($user)
                    ->setUpdate($this->update)
                    ->execute($callbackType, $chat->id);
                break;
            case BotMenuCallbackTypeEnum::PROMO_BRAND:
                $this->promoBrandCommand->setUser($user)
                    ->setUpdate($this->update)
                    ->execute($callbackType, $chat->id);
                break;
            case BotMenuCallbackTypeEnum::BALANCE:
                $this->balanceCommand->setUser($user)
                    ->execute($callbackType, $chat->id);
                break;
            case BotMenuCallbackTypeEnum::PROPOSAL:
                $this->proposalCommand->setUser($user)
                    ->setUpdate($this->update)
                    ->execute($callbackType, $chat->id);
                break;
            case BotMenuCallbackTypeEnum::COMPLAINT:
                $this->complaintCommand->setUser($user)
                    ->setUpdate($this->update)
                    ->execute($callbackType, $chat->id);
                break;

            case BotMenuCallbackTypeEnum::VISITS_AGENT:
                $this->visitsAgentCommand->setUser($user)
                    ->setUpdate($this->update)
                    ->execute($callbackType, $chat->id);
                break;
            case BotMenuCallbackTypeEnum::VISIT_PLAN:
                $this->visitPlanCommand->setUser($user)
                    ->execute($callbackType, $chat->id);
                break;
            case BotMenuCallbackTypeEnum::CONTACTS:
                $this->contactsCommand->setUser($user)
                    ->execute($callbackType, $chat->id);
                break;

            case BotMenuCallbackTypeEnum::REVIEW_VISIT:
                $this->reviewVisitCommand->setUser($user)
                    ->setUpdate($this->update)
                    ->execute($callbackType, $chat->id);
                break;
            case BotMenuCallbackTypeEnum::PRODUCT_CATEGORY:
                // Only for session
                break;
        }
    }

    public function setUser(?User $user): static
    {
        $this->user = $user;

        return $this;
    }

    public function setUpdate(UpdateObject $update): static
    {
        $this->update = $update;

        return $this;
    }
}
