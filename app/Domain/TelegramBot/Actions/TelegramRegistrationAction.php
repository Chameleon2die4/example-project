<?php
declare(strict_types=1);

namespace App\Domain\TelegramBot\Actions;

use App\Domain\TelegramBot\Commands\Registration\QrCodeCommand;
use App\Domain\TelegramBot\Commands\Registration\RegistrationCommand;
use App\Domain\TelegramBot\Commands\Registration\StartCommand;
use App\Domain\TelegramBot\Commands\Registration\UserSmsCommand;
use App\Domain\TelegramBot\Commands\Registration\UserVerifyCommand;
use App\Domain\TelegramBot\Enums\RegistrationStepsEnum;
use App\Domain\TelegramBot\Enums\ResponseStatusEnum;
use App\Domain\TelegramBot\Events\TelegramRegistrationLogEvent;
use App\Models\ContactPerson;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Telegram\Bot\Objects\Update as UpdateObject;
use BotLanguage;
use Throwable;

class TelegramRegistrationAction
{
    private ?User $user;

    private UpdateObject $update;

    private mixed $entity;

    /**
     * @param StartCommand $startCommand
     * @param RegistrationCommand $registrationCommand
     * @param UserSmsCommand $userSmsCommand
     * @param UserVerifyCommand $userVerifyCommand
     * @param QrCodeCommand $qrCodeCommand
     */
    public function __construct(
        private readonly StartCommand        $startCommand,
        private readonly RegistrationCommand $registrationCommand,
        private readonly UserSmsCommand      $userSmsCommand,
        private readonly UserVerifyCommand   $userVerifyCommand,
        private readonly QrCodeCommand   $qrCodeCommand,
    ) { }

    /**
     * @param RegistrationStepsEnum|null $type
     *
     * @return void
     * @throws Throwable
     */
    public function execute(?RegistrationStepsEnum $type): void
    {
        if (!$type) {
            return;
        }

        $chat = $this->update->getChat();
        $message = $this->update->getMessage();
        $user = $this->user;

        BotLanguage::set($user);

        event(new TelegramRegistrationLogEvent([
            'status' => ResponseStatusEnum::SUCCESS->value,
            'user_id' => $user?->id,
            'chat_id' => $chat->id,
            'response' => json_encode($message->getRawResponse()),
            'step' => $type->value,
            'message' => $message->text,
        ]));

        switch ($type) {
            case RegistrationStepsEnum::START :
                $this->startCommand
                    ->execute($chat->id);
                break;
            case RegistrationStepsEnum::PROVIDER :
                $this->registrationCommand
                    ->execute($chat->id, $this->entity?->partners_code);
                break;
            case RegistrationStepsEnum::QR_CODES :
                $this->qrCodeCommand
                    ->setEntity($this->entity)
                    ->execute($chat->id);
                break;
            case RegistrationStepsEnum::PHONE :
                $this->userSmsCommand
                    ->setContactPerson($this->entity)
                    ->setMessage([
                        'text' => $message->text,
                        'botType' => $message->objectType(),
                        'response' => json_encode($message->getRawResponse()),
                    ])
                    ->execute($user);
                break;
            case RegistrationStepsEnum::SMS_VERIFY :
                $this->userVerifyCommand
                    ->execute($user, $this->update);
                break;
            case RegistrationStepsEnum::REGISTRATION_SUCCESS:
                //nothing
                break;
            case RegistrationStepsEnum::PROVIDER_VALIDATION:
                break;

        }
    }

    /**
     * @param User|null $user
     *
     * @return $this
     */
    public function setUser(?User $user): static
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @param UpdateObject $update
     *
     * @return $this
     */
    public function setUpdate(UpdateObject $update): static
    {
        $this->update = $update;

        return $this;
    }

    /**
     * @param Model|ContactPerson|Builder|null|mixed $entity
     *
     * @return $this
     */
    public function setEntity(mixed $entity): static
    {
        $this->entity = $entity;

        return $this;
    }
}
