<?php
declare(strict_types=1);

namespace App\Domain\TelegramBot\Actions\Log;

use App\Domain\Base\Actions\Contracts\AsExecuteActionInterface;
use App\Models\NotificationTradingPointLog;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Throwable;

class TelegramNotificationLog implements AsExecuteActionInterface
{
    private ?string $tradingPointCode;
    private ?int $chatId;
    private ?string $noticeCode;
    private ?string $message;
    private ?string $type;

    public function execute(): void
    {
        try {
            DB::beginTransaction();
            $notificationTradingPointLog = new NotificationTradingPointLog();
            $notificationTradingPointLog->chat_id = $this->chatId;
            $notificationTradingPointLog->trading_point_code = $this->tradingPointCode;
            $notificationTradingPointLog->notice_code = $this->noticeCode;
            $notificationTradingPointLog->message = $this->message;
            $notificationTradingPointLog->type = $this->type;
            $notificationTradingPointLog->save();
            DB::commit();
        } catch (Throwable $e) {
            Log::error(__CLASS__ . ' ' . $e->getMessage());
        }
    }

    public function setChatId(?int $chatId): static
    {
        $this->chatId = $chatId;

        return $this;
    }

    /**
     * @param string|null $tradingPointCode
     *
     * @return $this
     */
    public function setTradingPointCode(?string $tradingPointCode): static
    {
        $this->tradingPointCode = $tradingPointCode;

        return $this;
    }

    /**
     * @param string|null $noticeCode
     *
     * @return $this
     */
    public function setNoticeCode(?string $noticeCode): static
    {
        $this->noticeCode = $noticeCode;

        return $this;
    }

    /**
     * @param string|null $message
     *
     * @return $this
     */
    public function setMessage(?string $message): static
    {
        $this->message = $message;

        return $this;
    }

    /**
     * @param string|null $type
     *
     * @return $this
     */
    public function setType(?string $type): static
    {
        $this->type = $type;

        return $this;
    }
}
