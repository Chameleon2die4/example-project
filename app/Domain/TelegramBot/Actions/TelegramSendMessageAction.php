<?php
declare(strict_types=1);

namespace App\Domain\TelegramBot\Actions;

use App\Domain\Base\Actions\Contracts\AsExecuteReturnActionInterface;
use App\Models\User;
use Illuminate\Support\Facades\Log;
use Telegram\Bot\Api;
use Telegram\Bot\Exceptions\TelegramSDKException;
use Telegram\Bot\Keyboard\Keyboard;

class TelegramSendMessageAction implements AsExecuteReturnActionInterface
{
    private int $chatId;

    private string $text;

    private ?array $options = null;

    private ?User $user = null;

    /**
     * @return Api
     */
    public function execute(): Api
    {
        $telegram = new Api();

        try {
            $message = [
                'chat_id' => $this->chatId,
                'text' => $this->text,
                'parse_mode' => 'HTML',
            ];

            $telegram->sendMessage($this->parseMessage($message));
        }
        catch (TelegramSDKException $e) {
            if ($e->getCode() === 403 && $this->user) {
                $user = $this->user;
                $user->is_inactive = 1;
                $user->save();
            } else {
                Log::error($e);
                Log::error($this->user);
                Log::error(__CLASS__.': '.__METHOD__.'->'.$e->getMessage());
            }
        }

        return $telegram;
    }

    /**
     * @param int $chatId
     *
     * @return $this
     */
    public function setChatId(int $chatId): static
    {
        $this->chatId = $chatId;

        return $this;
    }

    /**
     * @param string $text
     *
     * @return $this
     */
    public function setText(string $text): static
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Please read example readme
     * @param array|null $options
     *
     * $options = [
     *       'params'             => // array[][] [  [ ['text' => 'Btn', 'callback_data' => 'key'], ['text' => 'Btn', 'callback_data' => 'key'] ], [ ['text' => 'Btn', 'callback_data' => 'key']]  ]
     *       'is_keyboard'        => // bool
     *       'is_inline'          => // bool
     *       'resize_keyboard'    => // bool
     *       'one_time_keyboard'  => // bool
     * ]
     *
     * @return $this
     */
    public function setOptions(?array $options = null): static
    {
        $this->options = $options;

        return $this;
    }

    /**
     * @param array $message
     *
     * @return array
     */
    private function parseMessage(array $message): array
    {
        if ($this->options && $this->options['is_keyboard']) {
            $params = [];

            foreach ($this->options['params'] as $key => $row) {
                foreach ($row as $item) {
                    $params[$key][] = Keyboard::inlineButton($item);
                }
            }

            $keyboard = Keyboard::make();

            if ($this->options['is_inline']) {
                $keyboard->inline();
            }

            $keyboard->setResizeKeyboard($this->options['resize_keyboard'] ?? false);
            $keyboard->setOneTimeKeyboard($this->options['one_time_keyboard'] ?? false);

            foreach ($params as $param) {
                $keyboard->row($param);
            }

            $message['reply_markup'] = $keyboard;
        }

        return $message;
    }

    /**
     * @param ?User $user
     * @return TelegramSendMessageAction
     */
    public function setUser(?User $user): TelegramSendMessageAction
    {
        $this->user = $user;

        return $this;
    }
}
