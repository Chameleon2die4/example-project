<?php
declare(strict_types=1);

namespace App\Domain\TelegramBot\Actions;

use App\Domain\Base\Actions\Contracts\AsExecuteActionInterface;
use App\Domain\TelegramBot\Enums\BotMenuCallbackTypeEnum;
use App\Domain\TelegramBot\Enums\ResponseStatusEnum;
use App\Domain\TelegramBot\Enums\RegistrationStepsEnum;
use App\Models\TelegramUserSession;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Throwable;

class TelegramUserSessionAction implements AsExecuteActionInterface
{
    private ResponseStatusEnum|null $status;

    private string|null $botType;

    private RegistrationStepsEnum|BotMenuCallbackTypeEnum|null $type;

    private string $response;

    private int $chatId;

    private ?string $message;

    /**
     * @return void
     * @throws Throwable
     */
    public function execute(): void
    {
        DB::beginTransaction();
        $currentDate = Carbon::now()->format('Y-m-d H:i:s');

        TelegramUserSession::query()->insert([
            'chat_id' => $this->chatId,
            'message' => $this->message,
            'type' => $this->type?->value,
            'bot_type' => $this->botType,
            'response' => $this->response,
            'status' => $this->status?->value,
            'created_at' => $currentDate,
            'updated_at' => $currentDate,
        ]);

        DB::commit();
    }

    /**
     * @param int $chatId
     *
     * @return $this
     */
    public function setChatId(int $chatId): static
    {
        $this->chatId = $chatId;

        return $this;
    }

    /**
     * @param string|null $message
     *
     * @return $this
     */
    public function setMessage(?string $message): static
    {
        $this->message = $message;

        return $this;
    }

    /**
     * @param RegistrationStepsEnum|BotMenuCallbackTypeEnum $type
     * @return $this
     */
    public function setType(RegistrationStepsEnum|BotMenuCallbackTypeEnum $type): static
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @param string|null $botType
     *
     * @return $this
     */
    public function setBotType(?string $botType): static
    {
        $this->botType = $botType;

        return $this;
    }

    /**
     * @param string $response
     *
     * @return $this
     */
    public function setResponse(string $response): static
    {
        $this->response = $response;

        return $this;
    }

    /**
     * @param ResponseStatusEnum $status
     *
     * @return $this
     */
    public function setStatus(ResponseStatusEnum $status): static
    {
        $this->status = $status;

        return $this;
    }
}
