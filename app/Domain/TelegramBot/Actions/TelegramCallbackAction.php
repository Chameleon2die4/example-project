<?php
declare(strict_types=1);

namespace App\Domain\TelegramBot\Actions;

use App\Domain\TelegramBot\Enums\BotMenuCallbackTypeEnum;
use App\Domain\TelegramBot\Enums\MainMenuEnum;

class TelegramCallbackAction
{
    /**
     * @param string|null $text
     *
     * @return BotMenuCallbackTypeEnum|null
     */
    public function getType(?string $text): ?BotMenuCallbackTypeEnum
    {
        $key = null;

        if (!$text) {
            return null;
        }

        switch ($text) {
            case trans('menu.lang_uz') :
                $key = BotMenuCallbackTypeEnum::LANG_UZ;
                break;
            case trans('menu.lang_ru') :
                $key = BotMenuCallbackTypeEnum::LANG_RU;
                break;
            case trans('menu.registration') :
                $key = BotMenuCallbackTypeEnum::REGISTRATION;
                break;
            case trans('menu.change_lang') :
                $key = BotMenuCallbackTypeEnum::CHANGE_LANG;
                break;
            case trans('menu.about_bot') :
                $key = BotMenuCallbackTypeEnum::ABOUT_BOT;
                break;
            case trans('menu.visits_agent') :
                $key = BotMenuCallbackTypeEnum::VISITS_AGENT;
                break;
            case trans('menu.bonus') :
                $key = BotMenuCallbackTypeEnum::BONUS;
                break;
            case trans('menu.main_menu') :
                $key = BotMenuCallbackTypeEnum::MAIN_MENU;
                break;
            case trans('menu.visit_plan') :
                $key = BotMenuCallbackTypeEnum::VISIT_PLAN;
                break;
            case trans('menu.contacts') :
                $key = BotMenuCallbackTypeEnum::CONTACTS;
                break;
            case trans('menu.special_proposition') :
                $key = BotMenuCallbackTypeEnum::PROMO_CATEGORY;
                break;
            case trans('menu.orders') :
                $key = BotMenuCallbackTypeEnum::ORDERS;
                break;
            case trans('menu.orders_in_work') :
                $key = BotMenuCallbackTypeEnum::ORDERS_WORK;
                break;
            case trans('menu.orders_last') :
                $key = BotMenuCallbackTypeEnum::ORDERS_LAST;
                break;
            case trans('menu.balance') :
                $key = BotMenuCallbackTypeEnum::BALANCE;
                break;
            case trans('menu.support') :
                $key = BotMenuCallbackTypeEnum::SUPPORT;
                break;
            case trans('menu.complaint') :
                $key = BotMenuCallbackTypeEnum::COMPLAINT;
                break;
            case trans('menu.proposal') :
                $key = BotMenuCallbackTypeEnum::PROPOSAL;
                break;
            case trans('menu.review_visit') :
                $key = BotMenuCallbackTypeEnum::REVIEW_VISIT;
                break;
        }

        return $key;
    }

    public function getMainMenuButton(?string $text): ?string {
        if (!$text) {
            return null;
        }

        foreach (MainMenuEnum::cases() as $button) {
            if (trans("menu.$button->value") === $text) {
                return $button->value;
            }
        }

        return null;
    }
}
