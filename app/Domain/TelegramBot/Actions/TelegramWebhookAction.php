<?php
declare(strict_types=1);

namespace App\Domain\TelegramBot\Actions;

use App\Domain\Base\Actions\Contracts\AsControllerActionableInterface;
use App\Models\Mongo\ButtonClick;
use BotLanguage;
use App\Domain\TelegramBot\Commands\Registration\ResolveTelegramSessionCommand;
use App\Domain\TelegramBot\Enums\BotMenuCallbackTypeEnum;
use App\Domain\TelegramBot\Enums\RegistrationStepsEnum;
use App\Domain\TelegramBot\Traits\TelegramUserSessionTrait;
use App\Models\TelegramUserSession;
use App\Models\User;
use Illuminate\Http\Request;
use Telegram\Bot\Api;
use Telegram\Bot\Exceptions\TelegramSDKException;
use Throwable;

class TelegramWebhookAction implements AsControllerActionableInterface
{
    use TelegramUserSessionTrait;

    private Api $telegram;

    /**
     * @param ResolveTelegramSessionCommand $resolveTelegramSessionCommand
     * @param TelegramRegistrationAction $telegramRegistrationAction
     * @param TelegramMenuAction $telegramMenuAction
     * @param TelegramCallbackAction $telegramCallbackAction
     */
    public function __construct(
        private readonly ResolveTelegramSessionCommand $resolveTelegramSessionCommand,
        private readonly TelegramRegistrationAction $telegramRegistrationAction,
        private readonly TelegramMenuAction $telegramMenuAction,
        private readonly TelegramCallbackAction $telegramCallbackAction,
    ) {
        $this->telegram = new Api();
    }

    /**
     * @param Request $request
     *
     * @return void
     * @throws TelegramSDKException
     * @throws Throwable
     */
    public function asController(Request $request)
    {
        $updates = $this->telegram->getWebhookUpdate();
        $chat = $updates->getChat();
        $message = $updates->getMessage();
        $type = null;

        if (!$message->isEmpty()) {
            $user = User::query()->where('chat_id', $chat->id)->first();

            if ($user) {
                BotLanguage::set($user);
            } else {
                $langSession = $this->getLastSession($chat->id, BotMenuCallbackTypeEnum::CHANGE_LANG->value);

                if ($langSession) {
                    BotLanguage::set($langSession->message);
                }
            }

            $callbackType = $this->telegramCallbackAction->getType($message->text);

            if ($message->text === '/start') {
                if ($chat->id) {
                    TelegramUserSession::where('chat_id', $chat->id)->delete();
                }

                if ($user && $user->verify) {
                    if ($user->is_inactive) {
                        $user->is_inactive = 0;
                        $user->save();
                    }

                    $callbackType = BotMenuCallbackTypeEnum::MAIN_MENU;
                } else {
                    $type = RegistrationStepsEnum::START;
                    $callbackType = null;
                }
            }

            $entity = $this->resolveTelegramSessionCommand
                ->setUser($user)
                ->setUpdate($updates)
                ->execute();
            $userSession = TelegramUserSession::whereChatId($chat->id)->latest()->first();

            if ($userSession) {
                $type = RegistrationStepsEnum::tryFrom($userSession->type);
            }

            if ($user && $user->verify === 1) {
                $type = RegistrationStepsEnum::REGISTRATION_SUCCESS;
            }

            if ($userSession && $userSession->type === RegistrationStepsEnum::PROVIDER_VALIDATION->value) {
                $callbackType = BotMenuCallbackTypeEnum::REGISTRATION;
            }

            if ($userSession && !$callbackType) {
                $callbackType = BotMenuCallbackTypeEnum::tryFrom($userSession->type);
            }

            if ($type !== RegistrationStepsEnum::REGISTRATION_SUCCESS && !$callbackType) {
                $this->telegramRegistrationAction
                    ->setUser($user)
                    ->setUpdate($updates)
                    ->setEntity($entity)
                    ->execute($type);
            }

            if ($callbackType instanceof BotMenuCallbackTypeEnum && !$entity) {
                $mainButton = $this->telegramCallbackAction->getMainMenuButton($message->text);
                if ($mainButton) {
                    ButtonClick::create([
                        'button' => $mainButton,
                        'user_id' => $user?->id,
                    ]);
                }

                $this->telegramMenuAction
                    ->setUser($user)
                    ->setUpdate($updates)
                    ->execute($callbackType);
            }
        }
    }
}
