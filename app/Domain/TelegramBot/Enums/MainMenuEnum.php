<?php

namespace App\Domain\TelegramBot\Enums;

enum MainMenuEnum: string
{
    case VISITS_AGENT = 'visits_agent';
    case BONUS = 'bonus';
    case ORDERS = 'orders';
    case SPECIAL_PROPOSITION = 'special_proposition';
    case SUPPORT = 'support';
    case BALANCE = 'balance';
}
