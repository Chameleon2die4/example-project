<?php
declare(strict_types=1);

namespace App\Domain\TelegramBot\Enums;

enum RegistrationStepsEnum : string
{
    case START = 'start';
    case PROVIDER_VALIDATION = 'provider_validation';
    case PROVIDER = 'provider';
    case QR_CODES = 'qr_codes';
    case PHONE = 'phone';
    case SMS_VERIFY = 'sms_verify';
    case REGISTRATION_SUCCESS = 'registration_success';
}
