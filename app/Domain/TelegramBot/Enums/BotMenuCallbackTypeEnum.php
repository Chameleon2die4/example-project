<?php
declare(strict_types=1);

namespace App\Domain\TelegramBot\Enums;

enum BotMenuCallbackTypeEnum: string
{
    case LANG_RU = 'lang_ru';
    case LANG_UZ = 'lang_uz';
    case REGISTRATION = 'registration';
    case CHANGE_LANG = 'change_lang';
    case ABOUT_BOT = 'about_bot';
    case VISITS_AGENT = 'visits_agent';
    case BONUS = 'bonus';
    case ORDERS = 'orders';
    case ORDERS_WORK = 'orders_work';
    case ORDERS_LAST = 'orders_old';
    case SPECIAL_PROPOSITION = 'special_proposition';
    case SUPPORT = 'support';
    case MAIN_MENU = 'main_menu';
    case VISIT_PLAN = 'visit_plan';
    case CONTACTS = 'contacts';
    case REVIEW_VISIT = 'review_visit';
    case PRODUCT_CATEGORY = 'product_category';
    case PROMO_CATEGORY = 'promo_category';
    case PROMO_BRAND = 'promo_brand';
    case BALANCE = 'balance';

    case COMPLAINT = 'complaint';
    case PROPOSAL = 'proposal';
}
