<?php
declare(strict_types=1);

namespace App\Domain\TelegramBot\Enums;

enum ResponseStatusEnum : string
{
    case SUCCESS = 'success';
    case ERROR = 'error';
}
