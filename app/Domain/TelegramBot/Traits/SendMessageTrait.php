<?php
declare(strict_types=1);

namespace App\Domain\TelegramBot\Traits;

trait SendMessageTrait
{
    /**
     * @var int
     */
    private int $chatId;

    /**
     * @param string $message
     *
     * @return void
     */
    public function sendMessage(string $message): void
    {
        $this->telegramSendMessageAction
            ->setChatId($this->chatId)
            ->setText($message)
            ->execute();
    }

    /**
     * @param string $message
     * @param array $options
     *
     * @return void
     */
    public function sendMessageWithOptions(string $message, array $options): void
    {
        $this->telegramSendMessageAction
            ->setOptions($options)
            ->setChatId($this->chatId)
            ->setText($message)
            ->execute();
    }
}
