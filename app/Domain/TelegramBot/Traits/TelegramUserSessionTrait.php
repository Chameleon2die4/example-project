<?php
declare(strict_types=1);

namespace App\Domain\TelegramBot\Traits;

use App\Domain\TelegramBot\Actions\TelegramUserSessionAction;
use App\Models\TelegramUserSession;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Throwable;

trait TelegramUserSessionTrait
{

    /**
     * @param array $sessionData
     *
     * @return void
     */
    public function setSession(array $sessionData): void
    {
        try {
            $session = new TelegramUserSessionAction();

            $session
                ->setMessage($sessionData['message'])
                ->setChatId((int)$sessionData['chatId'])
                ->setBotType($sessionData['botType'])
                ->setResponse($sessionData['response'])
                ->setStatus($sessionData['status'])
                ->setType($sessionData['type'])
                ->execute();
        } catch (Throwable $exception) {
            Log::error(__CLASS__ . ': ' . __METHOD__ . '->' . $exception->getMessage());
        }
    }

    /**
     * @param int $chatId
     * @param string $type
     * @return void
     */
    public function removeSessionByType(int $chatId, string $type): void {
        TelegramUserSession::whereChatId($chatId)->whereType($type)->delete();
    }

    /**
     * @param int $chatId
     * @param string|null $type
     * @return TelegramUserSession|null
     */
    public function getLastSession(int $chatId, ?string $type = null) {
        $query = TelegramUserSession::whereChatId($chatId);

        if ($type) {
            $query->whereType($type);
        }

        return $query->latest()->first();
    }

}
