<?php

namespace App\Domain\TelegramBot\Events;

use App\Domain\TelegramBot\Listeners\TelegramNotificationTradingPointListener;
use Illuminate\Foundation\Events\Dispatchable;

/**
 * @covers TelegramNotificationTradingPointListener
 */
class TelegramNotificationTradingPointEvent
{
    use Dispatchable;
}
