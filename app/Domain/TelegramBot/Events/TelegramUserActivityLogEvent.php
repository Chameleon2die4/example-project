<?php

namespace App\Domain\TelegramBot\Events;

use App\Domain\TelegramBot\Listeners\TelegramUserActivityLogListener;

class TelegramUserActivityLogEvent
{
    public array $data;

    /**
     * @covers TelegramUserActivityLogListener
     *
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }
}
