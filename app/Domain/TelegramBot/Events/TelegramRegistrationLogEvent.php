<?php

namespace App\Domain\TelegramBot\Events;

use App\Domain\TelegramBot\Listeners\TelegramRegistrationLogListener;

class TelegramRegistrationLogEvent
{
    public array $data;

    /**
     * @covers TelegramRegistrationLogListener
     *
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }
}
