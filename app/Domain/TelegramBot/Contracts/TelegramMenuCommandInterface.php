<?php
declare(strict_types=1);

namespace App\Domain\TelegramBot\Contracts;

use App\Domain\TelegramBot\Actions\TelegramSendMessageAction;
use App\Domain\TelegramBot\Enums\BotMenuCallbackTypeEnum;

interface TelegramMenuCommandInterface
{
    /**
     * @param BotMenuCallbackTypeEnum $type
     * @param int $chatId
     *
     * @return void
     */
    public function execute(BotMenuCallbackTypeEnum $type, int $chatId): void;

    /**
     * @param string $message
     *
     * @return void
     */
    public function sendMessage(string $message): void;
}
