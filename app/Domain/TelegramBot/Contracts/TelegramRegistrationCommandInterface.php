<?php
declare(strict_types=1);

namespace App\Domain\TelegramBot\Contracts;

interface TelegramRegistrationCommandInterface
{
    /**
     * @param int $chatId
     *
     * @return void
     */
    public function execute(int $chatId) : void;
}
