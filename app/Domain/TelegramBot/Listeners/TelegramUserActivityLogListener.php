<?php

namespace App\Domain\TelegramBot\Listeners;

use App\Domain\TelegramBot\Events\TelegramUserActivityLogEvent;
use App\Models\TelegramUserActivityLog;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Throwable;

class TelegramUserActivityLogListener
{
    /**
     * @param TelegramUserActivityLogEvent $event
     * TODO: refactor to Actions/Log
     * @return void
     */
    public function handle(TelegramUserActivityLogEvent $event): void
    {
        $currentDate = Carbon::now()->format('Y-m-d H:i:s');

        if (!$event->data['message']) {
            return;
        }

        try {
            DB::beginTransaction();
            $log = new TelegramUserActivityLog();
            $log->user_id = $event->data['user_id'] ? (int)$event->data['user_id'] : null;
            $log->chat_id = (int)$event->data['chat_id'];
            $log->response = $event->data['response'] ?? null;
            $log->step = $event->data['step'] ?? null;
            $log->message = $event->data['message'];
            $log->created_at = $currentDate;
            $log->updated_at = $currentDate;
            $log->save();
            DB::commit();
        } catch (Throwable $e) {
            $log = new TelegramUserActivityLog();
            $log->chat_id = (int)$event->data['chat_id'];
            $log->message = $event->data['message'];
            $log->response = $e->getMessage();
            $log->created_at = $currentDate;
            $log->updated_at = $currentDate;
            $log->save();
        }
    }
}
