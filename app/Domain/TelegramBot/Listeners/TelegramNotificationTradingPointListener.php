<?php

namespace App\Domain\TelegramBot\Listeners;

use App\Domain\TelegramBot\Events\TelegramNotificationTradingPointEvent;
use Illuminate\Support\Facades\Artisan;
use Throwable;

class TelegramNotificationTradingPointListener
{

    /**
     * @param TelegramNotificationTradingPointEvent $event
     *
     * @return void
     * @throws Throwable
     */
    public function handle(TelegramNotificationTradingPointEvent $event): void
    {
        Artisan::call( 'send:notifications' );
    }
}
