
### Bot menu example data when use @method sendMessageWithOptions
```
'params' => [
        [ ['text' => '🍄 Btn', 'callback_data' => 'key'], ['text' => '🍄 Btn', 'callback_data' => 'key'] ],
        [ ['text' => '🍄 Btn', 'callback_data' => 'key'] ]
    ], //required
'is_keyboard' => true, //required
'is_inline' => false, //required
'resize_keyboard' => false, //not required
'one_time_keyboard' => false, //not required
]
```
