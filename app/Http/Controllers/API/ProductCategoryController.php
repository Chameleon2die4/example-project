<?php

namespace App\Http\Controllers\API;

use App\Domain\ProductCategory\Actions\ProductCategoryImportAction;
use App\Http\Controllers\Controller;
use App\Models\ProductCategory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ProductCategoryController extends Controller
{

    /**
     * @OA\Post(
     *      path="/api/v1/product-category/import",
     *      tags={"ProductCategory"},
     *      summary="Import new product categories",
     *      operationId="importProductCategory",
     *      security={{"bearer_token":{}}},
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(
     *             type="array",
     *                  @OA\Items(
     *                       type="object",
     *                       @OA\Property(property="trading_agent_code", type="string", example="110667"),
     *                       @OA\Property(property="trade_point_code", type="string", example="00-00016185"),
     *                       @OA\Property(property="product_category", type="string", example="006-1 MAKFA - Мука"),
     *                       @OA\Property(property="chat_bot_product_category", type="string", example="Макфа"),
     *                   ),
     *         )
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description="Records created successfully",
     *         @OA\JsonContent(@OA\Property(property="message", type="string", example="Inserted 10 product categories."))
     *     ),
     *     @OA\Response(
     *         response=204,
     *         description="No records inserted",
     *         @OA\JsonContent(@OA\Property(property="message", type="string", example="No product categories inserted."))
     *     ),
     * )
     * @param Request $request
     * @param ProductCategoryImportAction $productCategoryImportAction
     * @return Response
     */
    public function import(Request $request, ProductCategoryImportAction $productCategoryImportAction)
    {
        return $productCategoryImportAction->asController($request);
    }

    /**
     * @OA\Post(
     *      path="/api/v1/product-category/truncate",
     *      tags={"ProductCategory"},
     *      summary="Delete all product categories",
     *      operationId="truncateProductCategory",
     *      security={{"bearer_token":{}}},
     *     @OA\Response(
     *         response=200,
     *         description="Product categories deleted successfully",
     *         @OA\JsonContent(@OA\Property(property="message", type="string", example="Product categories deleted!"))
     *     ),
     * )
     *
     * @return Response
     */
    public function truncate() {
        ProductCategory::truncate();

        return \response([
            'message' => "Product categories deleted!"
        ]);
    }

}
