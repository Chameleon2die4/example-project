<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use App\Collections\UserCollection;
use App\Models\Mongo\ButtonClick;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Nova\Actions\Actionable;
use Laravel\Sanctum\HasApiTokens;

/**
 * App\Models\User
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property \Illuminate\Support\Carbon|null $email_verified_at
 * @property string $password
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Sanctum\PersonalAccessToken[] $tokens
 * @property-read int|null $tokens_count
 * @method static \Database\Factories\UserFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User query()
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUpdatedAt($value)
 * @property string|null $code
 * @property int|null $chat_id
 * @property string|null $fio
 * @property string|null $tel
 * @property string|null $uniq_key
 * @property int $verify
 * @property int $status
 * @property int $is_admin
 * @method static \Illuminate\Database\Eloquent\Builder|User whereChatId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereFio($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereIsAdmin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereTel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUniqKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereVerify($value)
 * @property-read TradingPoint|null $point
 * @method static UserCollection|static[] all($columns = ['*'])
 * @method static UserCollection|static[] get($columns = ['*'])
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Review[] $reviews
 * @property-read int|null $reviews_count
 * @property string $language
 * @method static Builder|User whereLanguage($value)
 * @property int $is_inactive
 * @method static Builder|User whereIsInactive($value)
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \Laravel\Nova\Actions\ActionEvent> $actions
 * @property-read int|null $actions_count
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\NotificationTradingPointLog> $history_notices
 * @property-read int|null $history_notices_count
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\TelegramRegistrationLog> $registration_logs
 * @property-read int|null $registration_logs_count
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\TelegramUserSession> $sessions
 * @property-read int|null $sessions_count
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\NotificationTradingPointLog> $historyNotices
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\TelegramRegistrationLog> $registrationLogs
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\TelegramUserActivityLog> $activityLogs
 * @property-read int|null $activity_logs_count
 * @mixin \Eloquent
 */
class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, Actionable;

    protected $connection = 'mysql';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'email_verified_at',
        'is_admin',
        'verify',

        'code',
        'chat_id',
        'fio',
        'tel',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::deleting(function (User $model) {
            $model->sessions()->delete();
            $model->activityLogs()->delete();
            $model->registrationLogs()->delete();

            ButtonClick::where('user_id', $model->id)->delete();
        });
    }

    public function reviews() {
        return $this->hasMany(Review::class);
    }

    public function history_notices() {
        return $this->hasMany(NotificationTradingPointLog::class,'chat_id', 'chat_id');
    }

    public function registrationLogs() {
        return $this->hasMany(TelegramRegistrationLog::class,'chat_id', 'chat_id');
    }

    public function activityLogs() {
        return $this->hasMany(TelegramUserActivityLog::class,'chat_id', 'chat_id');
    }

    public function sessions() {
        return $this->hasMany(TelegramUserSession::class,'chat_id', 'chat_id');
    }


    public function isVerified(): bool
    {
        return (bool)$this->verify;
    }

    /**
     * @return TradingPoint|null
     */
    public function getPointAttribute(): TradingPoint|null {
        if ($this->code) {
            return TradingPoint::whereCode($this->code)->first();
        } else {
            return null;
        }
    }

    public function clearTelegramSession() {
        if ($this->chat_id) {
            TelegramUserSession::where('chat_id', $this->chat_id)->delete();
        }
    }

    /**
     * @param array<int, User> $models
     */
    public function newCollection(array $models = []): UserCollection
    {
        return new UserCollection($models);
    }

    public function toggleActive() {
        $this->is_inactive = !$this->is_inactive;
        $this->save();
    }

}
