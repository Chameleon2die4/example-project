<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ProductCategory
 *
 * @property int $id
 * @property string $name
 * @property string|null $trading_agent_code
 * @property int|null $agent_id
 * @property string|null $trade_point_code
 * @property int|null $point_id
 * @property string $full_name
 * @property-read \App\Models\TradeAgent|null $agent
 * @property-read \App\Models\TradingPoint|null $point
 * @method static \Illuminate\Database\Eloquent\Builder|ProductCategory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ProductCategory newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ProductCategory query()
 * @method static \Illuminate\Database\Eloquent\Builder|ProductCategory whereAgentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductCategory whereFullName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductCategory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductCategory whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductCategory wherePointId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductCategory whereTradePointCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductCategory whereTradingAgentCode($value)
 * @mixin \Eloquent
 */
class ProductCategory extends Model
{
    use HasFactory;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    protected $fillable = [
        'name',
        'trading_agent_code',
        'agent_id',
        'trade_point_code',
        'point_id',
        'full_name',
    ];

    public function agent() {
        return $this->belongsTo(TradeAgent::class, 'agent_id');
    }

    public function point() {
        return $this->belongsTo(TradingPoint::class, 'point_id');
    }

    /** @noinspection PhpUnused */
    public function setRelationships() {
        if (!$this->point_id) {
            $point = TradingPoint::whereCode($this->trade_point_code)->first();
            $this->point()->associate($point);
        }

        if ($this->agent_id) {
            $agent = TradeAgent::whereCode($this->trading_agent_code)->first();
            $this->agent()->associate($agent);
        }

        $this->save();
    }

}
