<?php

namespace App\Jobs;

use App\Domain\TelegramBot\Actions\Log\TelegramNotificationLog;
use App\Domain\TelegramBot\Commands\Special\NotificationCommand;
use App\Models\Notice;
use App\Models\User;
use Illuminate\Bus\Batchable;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\App;

class TelegramSendMessage implements ShouldQueue
{
    use Batchable, Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private string $message;
    private User $user;
    private NotificationCommand $notificationCommand;
    private TelegramNotificationLog $telegramNotificationLog;
    private ?Notice $notice;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $message, User $user, Notice $notice = null)
    {
        $this->message = $message;
        $this->user = $user;
        $this->notice = $notice;

        $this->notificationCommand = App::make(NotificationCommand::class);
        $this->telegramNotificationLog = App::make(TelegramNotificationLog::class);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $code = $this->notice?->code ?? 0;
        $sent = $this->user->history_notices()->where('notice_code', $code)->first();

        if (!$sent) {
            $this->telegramNotificationLog
                ->setChatId((int)$this->user->chat_id)
                ->setTradingPointCode($this->user->code)
                ->setNoticeCode($this->notice?->code)
                ->setMessage($this->message)
                ->setType('all')
                ->execute();

            $this->notificationCommand->execute($this->message, $this->user->chat_id, $this->user);
        }
    }
}
