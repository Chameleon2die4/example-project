<?php /** @noinspection PhpUnused */

namespace App\Imports;

use App\Models\ProductCategory;
use App\Models\TradeAgent;
use App\Models\TradingPoint;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\Eloquent\Model;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ProductCategoryImport implements ToModel, WithHeadingRow, WithBatchInserts, WithChunkReading, ShouldQueue
{
    use Importable;

    /**
    * @param array $row
    *
    * @return Model|null
    */
    public function model(array $row)
    {
        if (!empty($row['chat_bot_product_category']) && $row['chat_bot_product_category'] !== '#NULL!') {
            return new ProductCategory([
                'name' => $row['chat_bot_product_category'],
                'trading_agent_code' => $row['trading_agent_code'],
                'trade_point_code' => $row['trade_point_code'],
                'full_name' => $row['product_category'],
            ]);
        } else {
            return null;
        }
    }

    public function batchSize(): int
    {
        return 300;
    }

    public function chunkSize(): int
    {
        return 300;
    }

}
